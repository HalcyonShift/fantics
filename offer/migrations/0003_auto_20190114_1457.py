# Generated by Django 2.1.4 on 2019-01-14 14:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('offer', '0002_auto_20190109_1823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='offer',
            name='auction',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offers', to='auction.Auction'),
        ),
        migrations.AlterField(
            model_name='offer',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offers', to='category.Category'),
        ),
        migrations.AlterField(
            model_name='offer',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offers', to=settings.AUTH_USER_MODEL),
        ),
    ]
