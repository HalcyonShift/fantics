from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.shortcuts import reverse
from django.test import (TestCase, RequestFactory, Client, override_settings)
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from .helpers import ValidatingTokenGenerator
from .models import User
from .views import (RegisterView, ValidateView, LoginView, ProfileView)


class AccountTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

        self.user = User.objects.create_user("TestUser", "test.user@fantics.me", "password", is_validating=True,
                                             display_name="Test User")


class AccountModelTestCase(AccountTestCase):
    def test_user_str(self):
        self.assertEqual(self.user.display_name, str(self.user))

    @override_settings(EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend')
    def test_send_email_verification_is_validating(self):
        request = self.factory.get('/')
        request.user = self.user

        account_activation_token = ValidatingTokenGenerator()
        token = account_activation_token.make_token(self.user)
        uidb64 = urlsafe_base64_encode(force_bytes(self.user.pk)).decode()

        message = self.user.send_email_verification(request)
        url = "{}{}".format(settings.BASE_URL, reverse('account:validate', kwargs={'uidb64': uidb64, 'token': token}))

        self.assertIn(url, message)

    @override_settings(EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend')
    def test_send_email_verification_is_not_validating(self):
        self.user.is_validating = False
        self.user.save()

        request = self.factory.get('/')
        request.user = self.user

        self.assertFalse(self.user.send_email_verification(request))
