from django.core.mail import EmailMessage


def send_email(subject, message, from_email, to_email, reply_to):
    email = EmailMessage(subject, message, from_email, to=[to_email, ], reply_to=[reply_to, ])

    return email.send(fail_silently=True)
