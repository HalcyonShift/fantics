import os
import dj_database_url
import django_heroku
import sentry_sdk

from django.contrib import messages
from django.core.exceptions import ImproperlyConfigured

from hashids import Hashids
from sentry_sdk.integrations.django import DjangoIntegration


if os.path.isfile(os.path.join('.env')):
    try:
        with open(os.path.join('.env'), 'r') as env_file:
            for var in env_file.read().split("\n"):
                os.environ.setdefault(var.split('=')[0], var.split('=')[1])

    except FileNotFoundError:
        from django.core.exceptions import ImproperlyConfigured
        raise ImproperlyConfigured("Couldn't find an .env file")


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_URL = os.environ.get('BASE_URL')


try:
    SECRET_KEY = os.environ['SECRET_KEY']
except KeyError:
    raise ImproperlyConfigured("No secret key")


DEBUG = bool(int(os.environ.get('DEBUG', 0)))

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS').split(',')

APPEND_SLASH = True

LOGIN_URL = "/login/"

ENVIRONMENT = os.environ.get('ENVIRONMENT')

# Application definition

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'storages',
    'bootstrap4',
    'channels',
    'ckeditor',
    'django_cleanup',
    'django_q',
    'encrypted_model_fields',
    'hijack',
    'rest_framework',
    'taggit',
    'account',
    'adhoc',
    'api',
    'auction',
    'bid',
    'category',
    'core',
    'main',
    'message',
    'offer',
    'tags'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'core.middleware.CurrencyMiddleware',
    'account.middleware.AccountConsentCookieMiddleware',
    'auction.middleware.AuctionMiddleware',
]

if DEBUG:
    INSTALLED_APPS.append('debug_toolbar')
    INSTALLED_APPS.append('django_jenkins')
    MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')
    INTERNAL_IPS = [os.environ.get('ALLOWED_HOSTS')]

else:
    INSTALLED_APPS.append('cacheops')
    sentry_sdk.init(
        dsn="https://5cd0e9cd39fe4b87b35eba2eb2390d9d@sentry.io/1368747",
        integrations=[DjangoIntegration()]
    )

ROOT_URLCONF = 'fantics.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'fantics', 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'core.context_processors.environment_settings',
            ],
        },
    },
]

WSGI_APPLICATION = 'fantics.wsgi.application'


# Logging
LOGGING = {}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        },
        "KEY_PREFIX": "fantics"
    }
}


# Cookies
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = False
SESSION_COOKIE_PATH = "/"
SESSION_COOKIE_DOMAIN = os.environ.get('SESSION_COOKIE_DOMAIN', None)


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

if os.environ.get('DATABASE_ENGINE'):
    DATABASES = {
        'default': {
            'ENGINE': os.environ.get('DATABASE_ENGINE'),
            'NAME': os.environ.get('DATABASE_NAME'),
            'USER': os.environ.get('DATABASE_USER'),
            'PASSWORD': os.environ.get('DATABASE_PASSWORD'),
            'HOST': os.environ.get('DATABASE_HOST'),
            'PORT': int(os.environ.get('DATABASE_PORT', 3306)),
            'CONN_MAX_AGE': int(os.environ.get('CONN_MAX_AGE', 60)),
        }
    }
else:
    DATABASES = {
        'default': dj_database_url.config(conn_max_age=60)
    }

# Account
AUTH_USER_MODEL = 'account.User'

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

if ENVIRONMENT != 'local' and os.environ.get('AWS_ACCESS_KEY_ID'):
    DEFAULT_FILE_STORAGE = 'core.storage_backends.MediaStorage'
    AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')
    AWS_STORAGE_BUCKET_NAME = os.environ.get('S3_BUCKET_NAME')
    AWS_DEFAULT_ACL = None
    AWS_S3_OBJECT_PARAMETERS = {
        'CacheControl': 'max-age=86400',
    }
    AWS_S3_REGION_NAME = "us-east-1"
    AWS_S3_CUSTOM_DOMAIN = "s3.{}.amazonaws.com/{}".format(AWS_S3_REGION_NAME, AWS_STORAGE_BUCKET_NAME)

    MEDIA_URL = 'https://{}/{}/'.format(AWS_S3_CUSTOM_DOMAIN, 'media')

else:
    STATIC_URL = '/static/'
    MEDIA_URL = '/media/'

    STATIC_ROOT = os.path.join(BASE_DIR, 'fantics', 'static')
    MEDIA_ROOT = os.path.join(BASE_DIR, 'fantics', 'media')


# Message settings
MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}


# Email
EMAIL_BACKEND = os.environ.get('EMAIL_BACKEND')
DEFAULT_FROM_EMAIL = "botling@fantics.me"

if os.environ.get('EMAIL_HOST'):
    EMAIL_HOST = os.environ.get('EMAIL_HOST')
    EMAIL_PORT = int(os.environ.get('EMAIL_PORT', 587))
    EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
    EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
    EMAIL_USE_TLS = bool(int(os.environ.get('EMAIL_USE_TLS', 0)))
    EMAIL_USE_SSL = bool(int(os.environ.get('EMAIL_USE_SSL', 0)))


# Lib settings
ASGI_APPLICATION = "fantics.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [os.environ.get('REDIS_URL')] if os.environ.get('REDIS_URL') else [('127.0.0.1', 6379)],
        },
    },
}

BOOTSTRAP4 = {
    'include_jquery': True,
    'jquery_url': '//code.jquery.com/jquery-3.2.1.min.js',
    'required_css_class': 'required'
}

CKEDITOR_CONFIGS = {
    'default': {
        'language': 'en',
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Bold', 'Italic', 'Underline'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft',
             'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            ['RemoveFormat', 'Source']
        ]
    }
}

FIELD_ENCRYPTION_KEY = "ct-DLSBBEgPSYH1Turk_4EMr2v1MLxjZfIRgaMWae_4="

FANTICS_HASH = Hashids(salt="NARmBPkKhFLC$}4a>QAe{:YI7Mk}k:", min_length=8, alphabet='abcdefghijklmnopqrstuvwxyz')

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

TAGGIT_CASE_INSENSITIVE = True
TAGGIT_TAGS_FROM_STRING = 'tags.helpers.parse_tags'

OPEN_EXCHANGE_APP_ID = "18e914d5dac74e5a930cc51adc89aa8e"

Q_CLUSTER = {
    'name': 'fantics',
    'workers': 1,
    'recycle': 500,
    'timeout': 60,
    'compress': True,
    'save_limit': 250,
    'queue_limit': 500,
    'cpu_affinity': 1,
    'label': 'Django Q',
    'django_redis': 'default'
}

if ENVIRONMENT != 'local':
    django_heroku.settings(locals(), databases=False)

    CACHEOPS_REDIS = os.environ.get('REDIS_URL')
    CACHEOPS_DEGRADE_ON_FAILURE = True
    CACHEOPS = {
        '*.*': {'timeout': 60*60},
    }
