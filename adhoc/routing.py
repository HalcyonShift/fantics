from django.urls import path

from .consumers import ItemConsumer


websocket_urlpatterns = [
    path('item/<int:item_id>/', ItemConsumer),
]
