from django.contrib import messages
from django.shortcuts import (reverse, redirect)

from .models import Auction


def auction_owner_required(func):
    def wrap(request, *args, **kwargs):

        if not request.auction:
            messages.error(request, "Auction {} doesn't exist :(".format(kwargs.get('auction')))
            return redirect(reverse('main:home'))

        try:
            if request.user.is_authenticated and request.auction.user_id == request.user.id:
                return func(request, *args, **kwargs)
            else:
                messages.error(request, 'Wow, do you not have permission to do that')

        except AttributeError as e:
            print(e)
            messages.error(request, 'Something went a little pear-shaped, give it another go?')

        return redirect(reverse('main:home'))

    return wrap
