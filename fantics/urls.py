from django.conf import settings
from django.conf.urls.static import static
from django.urls import (include, path)


urlpatterns = [
    path('hijack/', include('hijack.urls', namespace='hijack')),
    # path('api/v1/', include('api.urls')),
    path('', include('main.urls', namespace="main")),
    path('', include('message.urls', namespace="message")),
    path('', include('account.urls', namespace="account")),
    path('', include('adhoc.urls', namespace="adhoc")),
    path('', include('auction.urls', namespace="auction")),
    path('', include('bid.urls', namespace="bid")),
    path('', include('category.urls', namespace="category")),
    path('', include('offer.urls', namespace="offer")),
]


if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)), ] \
        + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)\
        + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)\
        + urlpatterns
