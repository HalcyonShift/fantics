from django.contrib import messages
from django.shortcuts import (reverse, redirect)

from .models import Offer


def offer_or_auction_owner_required(func):
    def wrap(request, *args, **kwargs):
        try:
            offer = Offer.objects.get(pk=kwargs.get('pk'))
        except Offer.DoesNotExist:
            messages.error(request, "Offer doesn't exist")
            return redirect(reverse('main:home'))

        try:
            if request.user.is_authenticated \
                    and (offer.user_id == request.user.id or offer.auction.user_id == request.user.id):
                return func(request, *args, **kwargs)
            else:
                messages.error(request, 'Wow, do you not have permission to do that')

        except AttributeError as e:
            print(e)
            messages.error(request, 'Something went a little pear-shaped, give it another go?')

        return redirect(reverse('main:home'))

    return wrap


def offer_owner_required(func):
    def wrap(request, *args, **kwargs):
        try:
            offer = Offer.objects.get(pk=kwargs.get('pk'))
        except Offer.DoesNotExist:
            messages.error(request, "Offer doesn't exist")
            return redirect(reverse('main:home'))

        try:
            if request.user.is_authenticated and offer.user_id == request.user.id:
                return func(request, *args, **kwargs)
            else:
                messages.error(request, 'Wow, do you not have permission to do that')

        except AttributeError as e:
            print(e)
            messages.error(request, 'Something went a little pear-shaped, give it another go?')

        return redirect(reverse('main:home'))

    return wrap
