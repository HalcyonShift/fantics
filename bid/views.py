from django.conf import settings
from django.contrib import messages
from django.shortcuts import (reverse, redirect)
from django.utils import timezone
from django.utils.html import mark_safe
from django.utils.decorators import method_decorator
from django.views.generic import (ListView, DeleteView, UpdateView, TemplateView, RedirectView)

from auction.decorators import auction_owner_required
from auction.view_mixins import AuctionMixin
from offer.models import Offer

from .forms import BidAdministrationForm
from .models import Bid
from .view_mixins import (BidMixin, BidAdministrationMixin, BidRedirectMixin)


class BidListView(BidMixin, AuctionMixin, ListView):
    model = Offer
    template_name = 'bid/list.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not self.auction.show_nav_bids \
                or self.kwargs.get('bidder') != self.request.user.slug:
            return redirect(reverse('auction:auction', kwargs={'user': self.request.auction.user.slug,
                                                               'auction': self.request.auction.slug}))

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return self.auction.offers.filter(bids__user=self.request.user).prefetch_related('bids').distinct()

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        pks = [o.pk for o in data['offer_list']]

        # fetch again, because our offer_list only has bids related to this user and we need all of them

        offers = Offer.objects.filter(pk__in=pks).prefetch_related('bids')

        for offer in data['offer_list']:
            for offer_with_bids in offers:
                if offer.pk == offer_with_bids.pk:
                    offer.is_top_bidder = any([True for bid in offer_with_bids.top_bids
                                               if bid.user_id == self.request.user.id])

        return data


class BidWinView(BidMixin, AuctionMixin, TemplateView):
    template_name = 'bid/wins.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        filters = dict.fromkeys(Bid.MODES, 0)

        wins = [bid for bid in self.request.auction.top_bids if bid.user_id == self.request.user.id]

        for bid in wins:
            if bid.user_id == self.request.user.id:
                filters[bid.status] += 1

        filters.update({'all': len(wins)})

        if self.request.GET.get('filter') in Bid.MODES:
            wins = [bid for bid in wins if bid.status == self.request.GET.get('filter')]

        data.update({'wins': wins, 'filters': filters})

        return data

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or not self.auction.show_nav_wins \
                or self.kwargs.get('bidder') != self.request.user.slug:
            return redirect(reverse('auction:auction', kwargs={'user': self.request.auction.user.slug,
                                                               'auction': self.request.auction.slug}))

        return super().dispatch(request, *args, **kwargs)


class BidDonationView(BidMixin, AuctionMixin, UpdateView):
    model = Bid
    template_name = 'bid/donate.html'
    fields = ['receipt', ]

    def get_object(self, queryset=None):
        try:
            return Bid.objects.get(pk=settings.FANTICS_HASH.decode(self.kwargs.get('hash'))[0])
        except (IndexError, Bid.DoesNotExist):
            messages.error(self.request, "Sorry, that bid hasn't been found - if the donation deadline has passed "
                                         "it may have been deleted")

            return redirect(reverse('bid:wins', kwargs={'user': self.request.auction.user.slug,
                                                        'auction': self.request.auction.slug,
                                                        'bidder': self.request.user.slug}))

    def form_valid(self, form):
        form.instance.donated = timezone.now()

        messages.success(self.request, "Thank you for your donation! On verification, you will be sent an e-mail with"
                                       "the auctionee's details.")

        message = mark_safe("<p>{} has made a donation, to view and validate go to {}{}</p>".format(
            form.instance.user.display_name,
            settings.BASE_URL,
            reverse('bid:update', kwargs={'user': self.request.auction.user.slug,
                                          'auction': self.request.auction.slug,
                                          'pk': form.instance.pk})
        ))

        self.request.auction.user.email_user("A donation has been made!", message)

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('bid:wins', kwargs={'user': self.request.auction.user.slug, 'auction': self.request.auction.slug,
                                           'bidder': self.object.user.slug})

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)

        if not self.auction.show_nav_wins:
            return redirect(reverse('auction:auction', kwargs={'user': self.request.auction.user.slug,
                                                               'auction': self.request.auction.slug}))

        elif not self.object.receipt and not self.object.can_donate(request.auction.top_bids):
            messages.error(self.request, "Sorry, this a donation receipt can't be added to this bid, either because "
                                         "the deadline has passed or because it's not a winning bid")

            if self.object.user_id == request.user.id:
                return redirect(reverse('bid:wins', kwargs={'user': self.request.auction.user.slug,
                                                            'auction': self.request.auction.slug,
                                                            'bidder': self.request.user.slug}))
            else:
                return redirect(reverse('auction:auction', kwargs={'user': self.request.auction.user.slug,
                                                                   'auction': self.request.auction.slug}))

        return response


@method_decorator(auction_owner_required, name='dispatch')
class BidEmailView(AuctionMixin, BidAdministrationMixin, UpdateView):
    model = Bid
    fields = ['amount', ]
    template_name = 'bid/email.html'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        data.update({
            'winner_donation_alert': self.object.winner_alert_email or self.object.generate_winner_donation_alert(),
            'email_auctionee_details_to_winner': self.object.winner_sent_auctionee_details_email or self.object.generate_winner_sent_auctionee_details_email(),
            'email_winner_details_to_auctionee': self.object.auctionee_sent_winner_details_email or self.object.generate_auctionee_sent_winner_details_email(),
        })

        if self.object.pinch_hitter:
            data.update({
                'email_pinch_hitter_details_to_winner': self.object.winner_sent_pinch_hitter_details_email or self.object.generate_winner_sent_pinch_hitter_details_email(),
                'email_winner_details_to_pinch_hitter': self.object.pinch_hitter_sent_winner_details_email or self.object.generate_pinch_hitter_sent_winner_details_email()
            })
        
        return data


@method_decorator(auction_owner_required, name='dispatch')
class BidUpdateView(AuctionMixin, BidAdministrationMixin, UpdateView):
    model = Bid
    template_name = 'bid/update.html'
    form_class = BidAdministrationForm

    def form_valid(self, form):
        messages.success(self.request, "The bid has been updated")

        if form.instance.has_donated and not form.instance.donated:
            form.instance.donated = timezone.now()

        elif not form.instance.has_donated:
            form.instance.donated = None

        if form.instance.has_donated and not form.instance.winner_sent_auctionee_details_email_date:
            form.instance.winner_sent_auctionee_details_email = form.instance.generate_winner_sent_auctionee_details_email()
            form.instance.winner_sent_auctionee_details_email_date = timezone.now()
            form.instance.email_auctionee_details_to_winner()

        if form.instance.has_donated and not form.instance.auctionee_sent_winner_details_email_date:
            form.instance.auctionee_sent_winner_details_email = form.instance.generate_auctionee_sent_winner_details_email()
            form.instance.auctionee_sent_winner_details_email_date = timezone.now()
            form.instance.email_winner_details_to_auctionee()

        if form.instance.pinch_hitter and not form.instance.winner_sent_pinch_hitter_details_email_date:
            form.instance.winner_sent_pinch_hitter_details_email = form.instance.generate_winner_sent_pinch_hitter_details_email()
            form.instance.winner_sent_pinch_hitter_details_email_date = timezone.now()
            form.instance.email_pinch_hitter_details_to_winner()

        if form.instance.pinch_hitter and not form.instance.pinch_hitter_sent_winner_details_email_date:
            form.instance.pinch_hitter_sent_winner_details_email = form.instance.generate_pinch_hitter_sent_winner_details_email()
            form.instance.pinch_hitter_sent_winner_details_email_date = timezone.now()
            form.instance.email_winner_details_to_pinch_hitter()

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('auction:results', kwargs={'user': self.request.auction.user.slug,
                                                  'auction': self.request.auction.slug})


@method_decorator(auction_owner_required, name='dispatch')
class BidDeleteView(AuctionMixin, DeleteView):
    model = Bid
    template_name = 'bid/delete.html'

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)

        if self.object.offer.auction_id != request.auction.id or self.object.donation_complete:
            messages.error(request, "You can't delete this bid")
            return reverse('auction:results', kwargs={'user': self.request.auction.user.slug,
                                                      'auction': self.request.auction.slug})

        return response

    def get_success_url(self):
        return reverse('auction:results', kwargs={'user': self.request.auction.user.slug,
                                                  'auction': self.request.auction.slug})


@method_decorator(auction_owner_required, name='dispatch')
class BidValidateView(BidRedirectMixin, RedirectView):
    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)

        if not self.bid.has_donated:
            if not self.bid.donated:
                self.bid.donated = timezone.now()

            self.bid.has_donated = True

        messages.success(request, "The bid has been flagged as donated for, and confirmation e-mails have been "
                                  "sent to {} and {} if they have not already been sent".format(
            self.bid.user.display_name, self.bid.offer.user.display_name))

        if not self.bid.winner_sent_auctionee_details_email_date:
            self.bid.winner_sent_auctionee_details_email = self.bid.generate_winner_sent_auctionee_details_email()
            self.bid.winner_sent_auctionee_details_email_date = timezone.now()
            self.bid.email_auctionee_details_to_winner()

        if not self.bid.auctionee_sent_winner_details_email_date:
            self.bid.auctionee_sent_winner_details_email = self.bid.generate_auctionee_sent_winner_details_email()
            self.bid.auctionee_sent_winner_details_email_date = timezone.now()
            self.bid.email_winner_details_to_auctionee()

        self.bid.save()

        return response


@method_decorator(auction_owner_required, name='dispatch')
class BidRemovePinchHitterView(BidRedirectMixin, RedirectView):
    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)

        if self.bid.pinch_hitter:
            messages.success(request, "{} is no longer pinch-hitter on this offer".format(self.bid.pinch_hitter))

            self.bid.pinch_hitter = None
            self.bid.pinch_hitter_sent_winner_details_email_date = None
            self.bid.winner_sent_pinch_hitter_details_email_date = None
            self.bid.pinch_hitter_sent_winner_details_email = ""
            self.bid.winner_sent_pinch_hitter_details_email = ""

            self.bid.save()

        return response

    def get_redirect_url(self, *args, **kwargs):
        return reverse('bid:update', kwargs={'user': self.request.auction.user.slug,
                                             'auction': self.request.auction.slug,
                                             'pk': self.bid.pk})


@method_decorator(auction_owner_required, name='dispatch')
class BidAlertEmailView(BidRedirectMixin, RedirectView):
    def get(self, request, *args, **kwargs):
        messages.success(request, "{} has been sent their alert e-mail".format(self.bid.user.display_name))

        self.bid.winner_alert_email = self.bid.generate_winner_donation_alert()
        self.bid.winner_alert_email_date = timezone.now()
        self.bid.save()

        self.bid.email_winner_donation_alert()

        return super().get(request, *args, **kwargs)


@method_decorator(auction_owner_required, name='dispatch')
class BidConfirmEmailView(BidRedirectMixin, RedirectView):
    to_winner = False
    to_auctionee = False

    def get(self, request, *args, **kwargs):
        messages.success(request, "{} has been sent a confirmation e-mail".format(
            self.bid.user.display_name if self.to_winner else self.bid.offer.user.display_name
        ))

        # send to winner
        if self.to_winner:
            if self.bid.pinch_hitter:
                self.bid.winner_sent_pinch_hitter_details_email = self.bid.generate_winner_sent_pinch_hitter_details_email()
                self.bid.winner_sent_pinch_hitter_details_email_date = timezone.now()
                self.bid.save()

                self.bid.email_pinch_hitter_details_to_winner()

            else:
                self.bid.winner_sent_auctionee_details_email = self.bid.generate_winner_sent_auctionee_details_email()
                self.bid.winner_sent_auctionee_details_email_date = timezone.now()
                self.bid.save()
            
                self.bid.email_auctionee_details_to_winner()
        
        # send to auctionee or pinch hitter
        elif self.to_auctionee:
            if self.bid.pinch_hitter:
                self.bid.pinch_hitter_sent_winner_details_email = self.bid.generate_pinch_hitter_sent_winner_details_email()
                self.bid.pinch_hitter_sent_winner_details_email_date = timezone.now()
                self.bid.save()

                self.bid.email_winner_details_to_pinch_hitter()

            else:
                self.bid.auctionee_sent_winner_details_email = self.bid.generate_auctionee_sent_winner_details_email()
                self.bid.auctionee_sent_winner_details_email_date = timezone.now()
                self.bid.save()

                self.bid.email_winner_details_to_auctionee()

        return super().get(request, *args, **kwargs)


@method_decorator(auction_owner_required, name='dispatch')
class BidBulkAlertEmailView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return reverse('auction:results', kwargs={'user': self.request.auction.user.slug,
                                                  'auction': self.request.auction.slug})

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)

        if not request.auction.can_send_email_winner_alerts:
            messages.error(request, "The bulk alert can't be sent for this auction")
            return response

        count = 0

        for bid in request.auction.top_bids:
            if bid.pending_alert:
                bid.winner_alert_email = bid.generate_winner_donation_alert()
                bid.winner_alert_email_date = timezone.now()
                bid.save()

                bid.email_winner_donation_alert(async=True)

                count += 1

        messages.success(request, "{} e-mails have been sent!".format(count))

        return response
