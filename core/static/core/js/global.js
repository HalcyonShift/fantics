/* show / hide password input as text */

$('.show-hide-password .input-group-text').on('click', function(e){
    var password_input = $($(this).parent().parent().find('input')[0]);
    var icon = $($(this).find('i.fa')[0]);

    if (password_input.attr('type') === 'text') {
        password_input.attr('type', 'password');
        icon.removeClass('fa-eye').addClass('fa-eye-slash');
    } else {
        password_input.attr('type', 'text');
        icon.removeClass('fa-eye-slash').addClass('fa-eye');
    }
});


/* tags */

var tag_input = $('input[data-role="tagsinput"]');

if (tag_input) {
    update_input = function() {
        value = [];
        jQuery.each(tag_input_bin.find('.tag'), function(k, v){
             value.push($(v).attr('data-tag'));
        });
        tag_input.attr('value', value.join(','));
    };

    delete_tag = function() {
        $(this).parent().remove();
        tag_input_bin.sortable('refresh');
        update_input();
    };

    add_tag = function(tag) {
        tag = tag.trim().replace(/\,/g, '').substr(0, 100).toLowerCase();

        if (tag_input_bin.find('.tag[data-tag="'+tag+'"]').length) {
            return;
        }

        tag = $("<span class='tag ui-state-default badge badge-secondary' data-tag='" + tag + "'>#" + tag + " <i class='fa fa-times'></i></span>");
        tag.find('.fa-times').on('click', delete_tag);
        tag_input_bin.append(tag);
        tag_input_bin.sortable('refresh');
        update_input();
    };

    /* create the temporary input */
    var tag_input_tmp = $("<input type='text' class='form-control' placeholder='"+tag_input.attr('placeholder')+"' id='id_tags_tmp' autocomplete='off'>");

    tag_input.parent().append(tag_input_tmp);
    tag_input.hide();

    /* create the div to display tags in + turn it into a sortable, because we're fancy that way */
    var tag_input_bin = $("<div class='tagsinput-bin'></div>");

    tag_input_bin.sortable({placeholder: "ui-state-highlight", stop: update_input});
    tag_input_bin.disableSelection();

    tag_input_tmp.parent().append(tag_input_bin);

    /* If we already have associated tags, we'll stick them in the bin */
    if (tag_input.val()) {
        jQuery.each(tag_input.val().split(','), function(t, tag) {
            add_tag(tag.replace(/\"/g, ''));
        });
    }

    /* And then we'll let others get created on key/blur/click/mental telepathy */
    tag_input_tmp.on('keydown blur click', function(e){
        if (e.which === 188 || e.which  === 13 || !e.which) {  // comma/return/click
            e.preventDefault();
            tag = $(this).val().trim().replace(',', '');
            if (tag) {
                add_tag(tag);
            }
            $(this).val('');
            return false;
        }
    });
}


$(function() {
    var cookie_form = $('#compliance-banner').find('form');
        cookie_form = $(cookie_form);

    cookie_form.submit(function(e){
        e.preventDefault();
        var data = cookie_form.serialize();
        $.post(cookie_form.attr('action'), data, function(){
            $('#compliance-banner').slideUp("slow");
        });
    });
});