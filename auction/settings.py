SIGNUP_EMAIL_SUBJECT = "Thank you for signing up!"
SIGNUP_EMAIL_TEMPLATE = "<p>Hi, {{auctionee}}!</p>" \
                        "<p>Thank you for offering {{quantity}}x {{category}}!</p>" \
                        "<p>You can update your offer at {{link}} until bidding starts.</p>"

BID_EMAIL_SUBJECT = "You've been bin on!"
BID_EMAIL_TEMPLATE = "<p>Hi, {{auctionee}}!</p>" \
                     "<p>{{bidder}} has bid {{amount}} for your {{category}}!</p>"


OVERBID_EMAIL_SUBJECT = "You've been overbid!"
OVERBID_EMAIL_TEMPLATE = "<p>Hi, {{bidder}}!</p>" \
                         "<p>You’ve been over-bid for {{auctionee}}! Go to {{link}} if you’d like to bid again.</p>"


WINNER_ALERT_EMAIL_SUBJECT = "You won!"
WINNER_ALERT_EMAIL_TEMPLATE = "<p>Congratulations, {{bidder}}!</p>" \
                              "<p>You’ve won {{auctionee}}’s {{category}} for {{amount}}.</p>" \
                              "<p>You have until {{deadline}} to make your donation by following the instructions below.</p>" \
                              "<p>{{donation_instructions}}</p>" \
                              "<p>{{donation_website}}</p>" \
                              "<p>After donating, please enter your receipt at {{link}}.</p>"

WINNER_DETAILS_TO_AUCTIONEE_EMAIL_SUBJECT = "You've been won!"
WINNER_DETAILS_TO_AUCTIONEE_EMAIL_TEMPLATE = "<p>Hi, {{auctionee}}!</p>" \
                                             "<p>Your {{category}} has been won by {{auctionee}} for {{amount}}.</p>" \
                                             "<p>You can contact them at {{email}}.</p>"


AUCTIONEE_DETAILS_TO_WINNER_EMAIL_SUBJECT = "Here's your auctionee!"
AUCTIONEE_DETAILS_TO_WINNER_EMAIL_TEMPLATE = "<p>Hi, {{bidder}}!</p>" \
                                             "<p>Thank you for your {{amount}} donation for {{auctionee}}’s {{category}}.</p>" \
                                             "<p>You can contact them at {{email}}.</p>"

WINNER_DETAILS_TO_PINCH_HITTER_EMAIL_SUBJECT = "Thank you for pinch-hitting!"
WINNER_DETAILS_TO_PINCH_HITTER_EMAIL_TEMPLATE = "<p>Hi, {{pinch_hitter}}!</p>" \
                                                "<p>Thank you for offering to pinch hit for {{category}} for {{auctionee}}.</p>" \
                                                "<p>You can contact them at {{email}}.</p>"

PINCH_HITTER_DETAILS_TO_WINNER_EMAIL_SUBJECT = "Here's your pinch-hitter!"
PINCH_HITTER_DETAILS_TO_WINNER_EMAIL_TEMPLATE = "<p>Hi, {{bidder}}!</p>" \
                                                "<p>{{pinch_hitter}} is taking over from {{auctionee}} for their {{category}}.</p>" \
                                                "<p>You can contact them at {{email}}.</p>"

BLACK_LIST = [
    'auction',
    'auction-',
    'create-auction',

]