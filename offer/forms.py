from django import forms

from tags.fields import FanticsTagField

from .models import Offer


class OfferForm(forms.ModelForm):
    deadline = forms.DateField(input_formats=["%m/%d/%Y", ], required=False)
    information_website = forms.URLField(widget=forms.URLInput(attrs={'placeholder': 'http://'}), required=False)
    tags = FanticsTagField(required=False)
    minimum_bid = forms.IntegerField(widget=forms.NumberInput(attrs={'step': 0.01, 'min': 1}))
    quantity = forms.IntegerField(widget=forms.NumberInput(attrs={'step': 1, 'min': 1}))

    class Meta:
        model = Offer
        fields = ['category', 'notes', 'deadline', 'minimum_bid', 'quantity', 'tags', 'image', 'information_website']

    def __init__(self, **kwargs):
        self.auction = None

        if 'auction' in kwargs:
            self.auction = kwargs.pop('auction')

        super().__init__(**kwargs)

        if self.auction and self.auction.max_quantity_per_offer:
            self.fields['quantity'] = forms.IntegerField(
                widget=forms.Select(choices=[(x+1, x+1) for x in range(self.auction.max_quantity_per_offer)])
            )

        self.fields['minimum_bid'].label = "Minimum bid ({})".format(self.auction.currency)

    def clean_deadline(self):
        if self.cleaned_data.get('deadline') and self.cleaned_data.get('deadline') < self.auction.bidding_ends.date():
            raise forms.ValidationError('The deadline must be after bidding ends', 'invalid', params=['deadline', ])

        return self.cleaned_data.get('deadline')

    def clean_minimum_bid(self):
        if not self.cleaned_data.get('minimum_bid') or self.cleaned_data.get('minimum_bid') < 1:
            self.cleaned_data.update({'minimum_bid': 1})

        return self.cleaned_data.get('minimum_bid')


class SearchForm(forms.Form):
    search = forms.CharField(widget=forms.TextInput(attrs={'type': 'search', 'placeholder': 'Keywords'}))
