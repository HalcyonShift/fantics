from django.urls import path

from .views import (LoginView, LogoutView, RegisterView, ValidateView, ProfileView, ResendValidationView,
                    PasswordResetView, PasswordResetConfirmView, CookiesView, PasswordChangeView, AccountDeleteView,
                    AccountUpdateView)


app_name = 'account'

urlpatterns = [
    path('create-account/', RegisterView.as_view(), name='register'),
    path('validate/<slug:uidb64>/<slug:token>/', ValidateView.as_view(), name='validate'),
    path('validate/', ResendValidationView.as_view(), name='resend_validate'),
    path('login/', LoginView.as_view(), name='login'),
    path('login/password/', PasswordResetView.as_view(), name='password_reset'),
    path('login/password/<slug:uidb64>/<slug:token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('account/', AccountUpdateView.as_view(), name='update'),
    path('account/password/', PasswordChangeView.as_view(), name='password'),
    path('account/delete/', AccountDeleteView.as_view(), name='delete'),
    path('cookies/', CookiesView.as_view(), name='cookies'),
    path('<slug:slug>/', ProfileView.as_view(), name='profile'),
]
