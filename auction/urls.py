from django.urls import path

from .views import (AuctionView, AuctionAboutView, AuctionCreateView, AuctionUpdateView, AuctionDeleteView,
                    AuctionResultsView, AuctionUpdateEmailView, AuctionUpdateLayoutView)


app_name = 'auction'

urlpatterns = [
    path('<slug:user>/create-auction/multi/', AuctionCreateView.as_view(), name='create'),
    path('<slug:user>/<slug:auction>/', AuctionView.as_view(), name='auction'),
    path('<slug:user>/<slug:auction>/about/', AuctionAboutView.as_view(), name='about'),
    path('<slug:user>/<slug:auction>/edit/', AuctionUpdateView.as_view(), name='update'),
    path('<slug:user>/<slug:auction>/edit/email/', AuctionUpdateEmailView.as_view(), name='update_email'),
    path('<slug:user>/<slug:auction>/edit/layout/', AuctionUpdateLayoutView.as_view(), name='update_layout'),
    path('<slug:user>/<slug:auction>/delete/', AuctionDeleteView.as_view(), name='delete'),
    path('<slug:user>/<slug:auction>/winning-bids/', AuctionResultsView.as_view(), name='results'),
]