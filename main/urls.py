from django.urls import path
from django.views.generic import TemplateView

from .views import (HomeView, AuctionModeView)


app_name = 'main'

urlpatterns = [
    path('create-auction/', AuctionModeView.as_view(), name='create'),
    path('about/', TemplateView.as_view(template_name='main/brochure/about.html'), name='about'),
    path('support/', TemplateView.as_view(template_name='main/brochure/support.html'), name='support'),
    path('privacy-policy/', TemplateView.as_view(template_name='main/brochure/privacy_policy.html'),
         name='privacy_policy'),
    path('terms-of-use/', TemplateView.as_view(template_name='main/brochure/terms_of_use.html'), name='terms_of_use'),
    path('', HomeView.as_view(), name='home')
]
