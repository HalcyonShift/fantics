from django import forms
from django.urls import reverse_lazy
from django.utils.html import mark_safe

from core.templatetags.core_tags import format_currency

from .models import (Bid, Item)


class BidForm(forms.ModelForm):
    class Meta:
        model = Bid
        fields = ['amount', 'anonymous']

    def __init__(self, **kwargs):
        if 'must_bid' in kwargs:
            self.must_bid = kwargs.pop('must_bid')
            self.currency = kwargs.pop('currency')

        super().__init__(**kwargs)

    def clean_amount(self):
        if self.cleaned_data.get('amount') < self.must_bid:
            raise forms.ValidationError(
                "You must bid at least {}".format(format_currency(self.must_bid, self.currency)),
                'invalid',
                params=['amount']
            )

        return self.cleaned_data.get('amount')


class ItemForm(forms.ModelForm):
    currency = forms.ChoiceField(widget=forms.Select, choices=[])

    title = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'e.g., Fundraiser for Médecins Sans Frontières'
    }))

    beneficiary_name = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'e.g., Médecins Sans Frontières'
    }))

    beneficiary_website = forms.URLField(widget=forms.URLInput(attrs={
        'placeholder': 'http://'
    }), required=False)

    information_link = forms.URLField(widget=forms.URLInput(attrs={
        'placeholder': 'http://'
    }), required=False)

    bidding_starts = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)
    bidding_ends = forms.DateTimeField(input_formats=["%m/%d/%Y %I:%M %p", ], required=False)

    quantity = forms.IntegerField(widget=forms.NumberInput(attrs={'step': 1, 'min': 1}), initial=1)

    class Meta:
        model = Item
        fields = ['title', 'beneficiary_name', 'beneficiary_website', 'is_listed', 'currency', 'timezone',
                  'category', 'notes', 'information_link', 'quantity', 'minimum_bid', 'bidding_starts',
                  'bidding_ends']


class ItemCreateForm(ItemForm):
    label = mark_safe("I have read and agree with the <a href='/terms-of-use'>terms of use</a>, and understand this is "
                      "for testing purposes only.".format(reverse_lazy('main:terms_of_use')))

    confirm = forms.BooleanField(widget=forms.CheckboxInput, label=label)
