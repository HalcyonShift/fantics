from django.urls import path

from .views import (AdHocCreateView, AdHocUpdateView, AdHocDeleteView, AdHocView)


app_name = 'adhoc'

urlpatterns = [
    path('<slug:user>/auction-<slug:hash>/', AdHocView.as_view(), name='view'),
    path('<slug:user>/auction-<slug:hash>/edit/', AdHocUpdateView.as_view(), name='update'),
    path('<slug:user>/auction-<slug:hash>/delete/', AdHocDeleteView.as_view(), name='delete'),
    path('<slug:user>/create-auction/single/', AdHocCreateView.as_view(), name='create'),
]
