import json

from django import forms
from django.conf import settings
from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import (reverse, redirect)
from django.utils import timezone
from django.utils.html import mark_safe
from django.views.generic import (CreateView, UpdateView, DeleteView, DetailView)

from .models import Item
from .forms import (ItemForm, ItemCreateForm, BidForm)
from .view_mixins import ItemSingleObjectMixin


class AdHocView(ItemSingleObjectMixin, CreateView, DetailView):
    model = Item
    template_name = 'adhoc/view.html'
    form_class = BidForm

    def dispatch(self, request, *args, **kwargs):
        if not self.get_object():
            messages.error(request, "Sorry, that auction hasn't been found")

            return redirect(reverse('main:home'))

        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        obj = self.get_object()

        kwargs = super().get_form_kwargs()
        kwargs.update({
            'must_bid': obj.must_bid,
            'currency': obj.currency
        })

        return kwargs

    def form_invalid(self, form):
        try:
            message = "<br>".join([m[0]['message'] for m in json.loads(form.errors.as_json()).values()])

        except (ValueError, KeyError, AttributeError):
            message = 'An unknown error occured, please try again'

        if self.request.is_ajax():
            return JsonResponse({'success': False, 'message': message})

        obj = self.get_object()

        messages.error(self.request, mark_safe(message))

        return redirect(reverse('adhoc:view', kwargs={'user': obj.user.slug, 'hash': obj.hash}))

    def form_valid(self, form):
        obj = self.get_object()

        if timezone.now() > self.get_object().bidding_ends:
            form.add_error(None, 'Bidding has ended for this auction!')
            return self.form_invalid(form)

        form.instance.user = self.request.user
        form.instance.item = obj

        response = super().form_valid(form)

        bids = form.instance.item.bids.all()

        # email the auctionee
        obj.user.email_user("You've been bid on!",
                            "Go to {}{} to view".format(
                                settings.BASE_URL,
                                reverse('adhoc:view', kwargs={'user': obj.user.slug, 'hash': obj.hash})
                            ))

        try:
            # email bidder if they've just been overbid
            overbid = bids[form.instance.item.quantity]

            overbid.user.email_user("You've been over-bid for {}!".format(obj.title),
                                    "Go to {}{} to view".format(
                                        settings.BASE_URL,
                                        reverse('adhoc:view', kwargs={'user': obj.user.slug, 'hash': obj.hash})
                                    ))

        except IndexError:
            pass

        if self.request.is_ajax():
            return JsonResponse({'success': True})

        return response

    def get_initial(self):
        initial = super().get_initial()
        initial.update({'amount': self.get_object().must_bid})
        return initial

    def get_success_url(self):
        return reverse('adhoc:view', kwargs={'user': self.get_object().user.slug, 'hash': self.get_object().hash})


class AdHocCreateView(CreateView):
    model = Item
    form_class = ItemCreateForm
    template_name = 'adhoc/administration/create.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            messages.error(request, "You must be logged in to create an auction")
            return redirect(reverse('account:login'))

        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)

        form.fields['currency'].choices = [(c.code, c.code) for c in self.request.currency]

        return form

    def form_valid(self, form):
        form.instance.user = self.request.user

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('adhoc:view', kwargs={'user': self.request.user.slug, 'hash': self.object.hash})


class AdHocUpdateView(ItemSingleObjectMixin, UpdateView):
    model = Item
    form_class = ItemForm
    template_name = 'adhoc/administration/update.html'

    def dispatch(self, request, *args, **kwargs):
        if not self.get_object():
            messages.error(self.request, "Sorry, that auction hasn't been found")

            return redirect(reverse('main:home'))

        elif request.user.is_anonymous or request.user.id != self.get_object().user_id:
            messages.error(request, "You must be the owner of this auction to edit it")
            return redirect(reverse('account:login'))

        return super().dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        form.fields['currency'].choices = [(c.code, c.code) for c in self.request.currency]

        if self.object.count_bids:
            form.fields['currency'].widget = forms.HiddenInput()
            form.fields['timezone'].widget = forms.HiddenInput()
            form.fields['beneficiary_name'].widget = forms.HiddenInput()
            form.fields['category'].widget = forms.HiddenInput()
            form.fields['notes'].widget = forms.HiddenInput()
            form.fields['minimum_bid'].widget = forms.HiddenInput()

        return form

    def get_success_url(self):
        return reverse('adhoc:view', kwargs={'user': self.request.user.slug, 'hash': self.object.hash})


class AdHocDeleteView(ItemSingleObjectMixin, DeleteView):
    model = Item
    template_name = 'adhoc/administration/delete.html'

    def get_success_url(self):
        return reverse('account:profile', kwargs={'slug': self.request.user.slug})

    def dispatch(self, request, *args, **kwargs):
        if not self.get_object():
            messages.error(self.request, "Sorry, that auction hasn't been found")

            return redirect(reverse('main:home'))

        elif request.user.is_anonymous or request.user.id != self.get_object().user_id:
            messages.error(request, "You must be the owner of this auction to edit it")
            return redirect(reverse('account:login'))

        elif self.get_object().count_bids:
            messages.error(self.request, "Your auction has bids - it can't be deleted")
            return redirect(reverse('adhoc:update', kwargs={'user': self.get_object().user.slug,
                                                            'hash': self.get_object().hash}))

        return super().dispatch(request, *args, **kwargs)
