from django.urls import path

from .views import (MessageListView, MessageDeleteView, MessageCreateView, MessageView)


app_name = 'message'

urlpatterns = [
    path('send', MessageCreateView.as_view(), name='create'),
    path('inbox/<int:pk>/', MessageView.as_view(), name='view'),
    path('inbox/<int:pk>/delete', MessageDeleteView.as_view(), name='delete'),
    path('inbox/', MessageListView.as_view(), name='list')
]
