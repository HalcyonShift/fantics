from django.db import models
from django.db.models.functions import Lower

from autoslugged import AutoSlugField

from core.helpers import file_name


def category_file_name(instance, filename):
    return '/'.join(['auction', 'category', 'icons', file_name(filename)])


class CategoryManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()\
            .order_by(Lower('name')) \
            .annotate(
                count_offers=models.Count('offers'),
            )


class Category(models.Model):
    auction = models.ForeignKey('auction.Auction', related_name='categories', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='name', unique_with='auction', always_update=True)
    description = models.TextField(blank=True)
    icon = models.ImageField(null=True, blank=True, upload_to=category_file_name)

    objects = CategoryManager()

    def __str__(self):
        return self.name
