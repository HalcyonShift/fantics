from django.contrib.auth import login
from django.contrib.auth.views import (LoginView as _LoginView,
                                       PasswordResetView as _PasswordResetView,
                                       PasswordResetConfirmView as _PasswordResetConfirmView)
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.html import mark_safe
from django.views.generic import CreateView

from ..decorators import anonymous_required
from ..forms import (LoginForm, RegisterForm)
from ..models import User


@method_decorator(anonymous_required(redirect_url=reverse_lazy('main:home')), name='dispatch')
class LoginView(_LoginView):
    template_name = 'account/login.html'
    form_class = LoginForm

    def form_valid(self, form):
        login(self.request, form.get_user())

        if not form.cleaned_data.get('remember'):
            self.request.session.set_expiry(0)

        return super().form_valid(form)


@method_decorator(anonymous_required(redirect_url=reverse_lazy('main:home')), name='dispatch')
class PasswordResetConfirmView(SuccessMessageMixin, _PasswordResetConfirmView):
    template_name = 'account/password_reset_confirm.html'
    success_url = reverse_lazy('account:update')
    post_reset_login = True

    def get_success_message(self, cleaned_data):
        return "Your password has been reset."


@method_decorator(anonymous_required(redirect_url=reverse_lazy('main:home')), name='dispatch')
class PasswordResetView(SuccessMessageMixin, _PasswordResetView):
    template_name = 'account/password_reset.html'
    email_template_name = 'account/emails/password_reset.html'
    success_url = reverse_lazy('account:login')

    def form_valid(self, form):
        try:
            user = User.objects.get(email=form.cleaned_data.get('email'))

        except User.DoesNotExist:
            form.add_error('email', "Sorry, that e-mail hasn't been found")
            return super().form_invalid(form)

        return super().form_valid(form)

    def get_success_message(self, cleaned_data):
        return "An e-mail is heading your way. In the meantime, here's the scenic log in form again."


@method_decorator(anonymous_required(redirect_url=reverse_lazy('main:home')), name='dispatch')
class RegisterView(SuccessMessageMixin, CreateView):
    template_name = 'account/register.html'
    form_class = RegisterForm
    model = User
    success_url = reverse_lazy('account:login')

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        form.fields['terms_of_use'].label = mark_safe("I agree with the <a href='{}' target='_blank'>terms of use</a> "
                                                      "and have read the <a href='{}' target='_blank'>privacy policy"
                                                      "</a>".format(reverse_lazy('main:terms_of_use'),
                                                                    reverse_lazy('main:privacy_policy')))
        return form

    def get_success_message(self, cleaned_data):
        return 'Your account has been created, you will receive a verification e-mail at {} shortly'.format(
            cleaned_data.get('email')
        )

    def form_valid(self, form):
        form.instance.is_validating = True
        form.instance.set_password(form.cleaned_data.get('password1'))

        response = super().form_valid(form)

        form.instance.send_email_verification(self.request)  # send after saving or no pk

        return response
