import json

from channels.generic.websocket import AsyncWebsocketConsumer

from core.templatetags.core_tags import format_currency
from .models import Item


class ItemConsumer(AsyncWebsocketConsumer):
    item_id = None
    item_group_name = None

    async def connect(self):
        self.item_id = self.scope['url_route']['kwargs']['item_id']
        self.item_group_name = 'item_{}'.format(self.item_id)

        await self.channel_layer.group_add(self.item_group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.item_group_name, self.channel_name)

    async def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)

        await self.channel_layer.group_send(
            self.item_group_name, {'type': 'bids', 'status': data.get('status', 0), 'message': data.get('message', '')}
        )

    async def bids(self, event):
        item = Item.objects.get(pk=self.item_id)

        top_bids = [{
            'amount': format_currency(bid.amount, item.currency),
            'user_id': bid.user_id,
            'anonymous': bid.anonymous,
            'display_name': bid.user.display_name
        } for bid in item.top_bids]

        await self.send(text_data=json.dumps({'must_bid': str(item.must_bid), 'top_bids': top_bids}))
