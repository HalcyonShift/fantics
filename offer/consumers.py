import json

from channels.generic.websocket import AsyncWebsocketConsumer

from core.templatetags.core_tags import format_currency
from .models import Offer


class OfferConsumer(AsyncWebsocketConsumer):
    offer_id = None
    offer_group_name = None

    async def connect(self):
        self.offer_id = self.scope['url_route']['kwargs']['offer_id']
        self.offer_group_name = 'offer_{}'.format(self.offer_id)

        await self.channel_layer.group_add(self.offer_group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.offer_group_name, self.channel_name)

    async def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)

        await self.channel_layer.group_send(
            self.offer_group_name, {'type': 'bids', 'status': data.get('status', 0), 'message': data.get('message', '')}
        )

    async def bids(self, event):
        offer = Offer.objects.get(pk=self.offer_id)

        top_bids = [{
            'amount': format_currency(bid.amount, offer.auction.currency),
            'user_id': bid.user_id,
            'anonymous': bid.anonymous,
            'display_name': bid.user.display_name
        } for bid in offer.top_bids]

        await self.send(text_data=json.dumps({'must_bid': str(offer.must_bid), 'top_bids': top_bids}))
