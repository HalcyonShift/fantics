# Generated by Django 2.1.4 on 2019-01-09 18:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bid', '0001_initial'),
        ('offer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bid',
            name='offer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='bids', to='offer.Offer'),
        ),
        migrations.AddField(
            model_name='bid',
            name='pinch_hitter',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='pinch_hits', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='bid',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='bids', to=settings.AUTH_USER_MODEL),
        ),
    ]
