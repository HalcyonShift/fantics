from django.contrib import messages
from django.shortcuts import (redirect, reverse)


class OfferMixin:
    def dispatch(self, request, *args, **kwargs):

        if request.user.is_anonymous:
            messages.info(request, 'You must be logged in to add an offer to this auction')
            return redirect('{}?next={}'.format(reverse('account:login'), reverse('offer:create', kwargs={
                'user': request.auction.user.slug, 'auction': request.auction.slug,
            })))

        elif not request.auction.signups_open and request.auction.user.id != request.user.id:
            messages.info(request, 'Sorry, offers cannot be added or edited for this auction')
            return redirect(reverse('offer:list', kwargs={
                'user': request.auction.user.slug,
                'auction': request.auction.slug,
                'auctionee': self.object.user.slug
            }))

        return super().dispatch(request, *args, **kwargs)

    def get_initial(self):
        initial = super().get_initial()
        if not self.object:
            initial.update({'quantity': 1, 'minimum_bid': 1.00})

        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'auction': self.request.auction})
        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class=form_class)
        form.fields['category'].queryset = self.request.auction.categories

        return form
