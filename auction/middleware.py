from pytz import timezone

from django.utils.timezone import activate

from .models import Auction


class AuctionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        if not hasattr(request, 'auction') and view_kwargs.get('auction') and view_kwargs.get('user'):
            request.auction = None

            try:
                request.auction = Auction.objects.filter(slug=view_kwargs.get('auction'),
                                                         user__slug=view_kwargs.get('user')).with_related_offers().get()
            except Auction.DoesNotExist:
                pass

            if request.auction and request.auction.timezone != "UTC":
                activate(timezone(request.auction.timezone))

        return None
