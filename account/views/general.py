from django.contrib import messages
from django.contrib.auth import (login, logout)
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.generic import (RedirectView, FormView, DetailView)

from ..forms import CookieForm
from ..helpers import ValidatingTokenGenerator
from ..models import User


class ProfileView(DetailView):
    template_name = 'account/profile.html'
    model = User
    
    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        
        auctions = self.object.auctions.all()
        adhoc_auctions = self.object.adhoc_auctions.all()
        
        data.update({
            'multi': [auction for auction in auctions
                      if auction.is_listed or auction.user_id == self.request.user.id],
            'single': [auction for auction in adhoc_auctions
                       if auction.is_listed or auction.user_id == self.request.user.id]
        })
        
        return data


class CookiesView(SuccessMessageMixin, FormView):
    template_name = 'account/cookies/settings.html'
    form_class = CookieForm
    success_message = 'Your cookie preferences have been updated'

    def form_valid(self, form):
        consent = [key for key in ['preferences', 'analytics'] if form.cleaned_data.get(key)]

        response = super().form_valid(form)
        response.set_cookie('cookie_consent', str(consent), max_age=31536000)

        return response

    def get_initial(self):
        try:
            return {
                'preferences': self.request.user.allows_preferences_cookies,
                'analytics': self.request.user.allows_analytics_cookies
            }
        except AttributeError:
            return {'preferences': False, 'analytics': False}

    def get_success_url(self):
        return reverse('account:cookies')


class ValidateView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return reverse('account:update')

    def get(self, *args, **kwargs):
        uidb64 = kwargs.get('uidb64', '')
        token = kwargs.get('token', '')

        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None

        account_validation_token = ValidatingTokenGenerator()

        if user is not None and account_validation_token.check_token(user, token):
            messages.success(self.request, 'That annoying validate banner will haunt you no more!')

            if self.request.user.is_authenticated:
                logout(self.request)

            user.is_active = True
            user.is_validating = False
            user.save()

            login(self.request, user)

        return super().get(*args, **kwargs)
