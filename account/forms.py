from django import forms
from django.contrib.auth.forms import (AuthenticationForm, UserCreationForm)
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

from .models import User
from .settings import BLACK_LIST


class AccountForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput)
    website = forms.URLField(widget=forms.URLInput(attrs={'placeholder': 'http://www.yourwebsite.com'}), required=False)

    class Meta:
        model = User
        fields = ['email', 'username', 'display_name', 'tumblr', 'website', 'twitter', 'pillowfort',
                  'dreamwidth', 'ao3', 'livejournal', 'icon']

    def clean_display_name(self):
        if slugify(self.cleaned_data.get('display_name')) in BLACK_LIST:
            raise forms.ValidationError("Sorry, you can't choose {} as a display name".format(
                self.cleaned_data.get('display_name')
            ), 'invalid', params=['display_name', ])

        user = User.objects.\
            filter(display_name__iexact=self.cleaned_data.get('display_name'))\
            .exclude(pk=self.instance.pk)

        if user.count():
            raise forms.ValidationError('A user with this display name already exists',
                                        'not_unique',
                                        params=['display_name', ])

        return self.cleaned_data.get('display_name')


class CookieForm(forms.Form):
    necessary = forms.BooleanField(widget=forms.CheckboxInput(attrs={'checked': True, 'disabled': True}),
                                   required=False)
    preferences = forms.BooleanField(widget=forms.CheckboxInput, required=False)
    analytics = forms.BooleanField(widget=forms.CheckboxInput, required=False)


class LoginForm(AuthenticationForm):
    remember = forms.BooleanField(widget=forms.CheckboxInput(), initial=False,
                                  label=_('Remember me'), required=False)


class RegisterForm(UserCreationForm):
    email = forms.EmailField(widget=forms.EmailInput())
    age = forms.BooleanField(widget=forms.CheckboxInput(), label="I'm 13 or older")
    terms_of_use = forms.BooleanField(widget=forms.CheckboxInput(), label="")

    def clean_display_name(self):
        if slugify(self.cleaned_data.get('display_name')) in BLACK_LIST:
            raise forms.ValidationError("Sorry, you can't choose {} as a display name".format(
                self.cleaned_data.get('display_name')
            ), 'invalid', params=['display_name', ])

        user = User.objects.filter(display_name__iexact=self.cleaned_data.get('display_name'))

        if user.count():
            raise forms.ValidationError('A user with this display name already exists',
                                        'not_unique',
                                        params=['display_name', ])

        return self.cleaned_data.get('display_name')

    class Meta:
        model = User
        fields = ('email', 'username', 'display_name', 'password1', 'password2', 'age', 'terms_of_use')
