from django.conf import settings

from .models import Item


class ItemSingleObjectMixin:
    _object = "clear"

    def get_object(self, queryset=None):
        if self._object != "clear":
            return self._object

        try:
            self._object = Item.objects.get(pk=settings.FANTICS_HASH.decode(self.kwargs.get('hash'))[0])
        except (IndexError, Item.DoesNotExist):
            self._object = None

        return self._object
