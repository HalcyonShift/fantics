import json

from django.conf import settings
from django.core.mail import EmailMessage
from django.contrib import messages
from django.http import JsonResponse
from django.shortcuts import (reverse, redirect)
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.html import mark_safe
from django.views.generic import (CreateView, UpdateView, DeleteView, DetailView, ListView, TemplateView, FormView)

from auction.decorators import auction_owner_required
from auction.views import AuctionView
from auction.view_mixins import AuctionMixin
from bid.forms import BidForm
from core.templatetags.core_tags import format_currency

from .decorators import offer_owner_required
from .forms import (OfferForm, SearchForm)
from .models import Offer
from .view_mixins import OfferMixin


class OfferView(AuctionMixin, CreateView, DetailView):
    model = Offer
    template_name = 'offer/view.html'
    form_class = BidForm

    def dispatch(self, request, *args, **kwargs):
        if not self.get_object():
            return redirect(reverse('auction:auction', kwargs={'user': self.request.auction.user.slug,
                                                               'auction': self.request.auction.slug}))

        return super().dispatch(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        for offer in self.request.auction.offers.all():
            if offer.pk == self.kwargs.get('pk'):
                return offer
            
        return None

    def get_form_kwargs(self):
        obj = self.get_object()

        kwargs = super().get_form_kwargs()
        kwargs.update({
            'must_bid': obj.must_bid,
            'currency': obj.auction.currency
        })

        return kwargs

    def form_invalid(self, form):
        try:
            message = "<br>".join([m[0]['message'] for m in json.loads(form.errors.as_json()).values()])

        except (ValueError, KeyError, AttributeError):
            message = 'An unknown error occured, please try again'

        if self.request.is_ajax():
            return JsonResponse({'success': False, 'message': message})

        obj = self.get_object()

        messages.error(self.request, mark_safe(message))

        return redirect(reverse('offer:view', kwargs={'auction': self.request.auction.slug,
                                                      'auctionee': obj.user.slug,
                                                      'pk': obj.pk,
                                                      'user': self.request.auction.user.slug}))

    def form_valid(self, form):
        if timezone.now() > self.request.auction.bidding_ends:
            form.add_error(None, 'Bidding has ended for this auction!')
            return self.form_invalid(form)

        form.instance.user = self.request.user
        form.instance.offer = self.get_object()

        response = super().form_valid(form)

        bids = form.instance.offer.bids.all()

        # email the auctionee
        email = EmailMessage(
            "[{}] {}".format(form.instance.offer.auction.title, form.instance.offer.auction.bid_email_subject),
            form.instance.offer.auction.bid_email_body.format(**{
                'auctionee': form.instance.offer.user.display_name,
                'category': form.instance.offer.category.name,
                'bidder': 'Anonymous' if form.instance.anonymous else form.instance.user.display_name,
                'amount': format_currency(form.instance.amount, self.request.auction.currency)
            }),
            "<Fantics> {}".format(settings.DEFAULT_FROM_EMAIL),
            to=[form.instance.offer.user.email],
            reply_to=["<{}> {}".format(form.instance.offer.auction.contact_name,
                                       form.instance.offer.auction.contact_email)]
        )

        email.send(fail_silently=True)

        try:
            # email bidder if they've just been overbid
            overbid = bids[form.instance.offer.quantity]

            email = EmailMessage(
                "[{}] {}".format(overbid.offer.auction.title, overbid.offer.auction.overbid_email_subject),
                overbid.offer.auction.overbid_email_body.format(**{
                    'auctionee': overbid.offer.user.display_name,
                    'bidder': overbid.user.display_name,
                    'link': "{}{}".format(settings.BASE_URL, reverse('offer:view', kwargs={
                        'user': overbid.offer.auction.user.slug,
                        'auction': overbid.offer.auction.slug,
                        'auctionee': overbid.offer.user.slug,
                        'pk': overbid.offer.pk
                    }))
                }),
                "<Fantics> {}".format(settings.DEFAULT_FROM_EMAIL),
                to=[overbid.user.email],
                reply_to=["<{}> {}".format(form.instance.offer.auction.contact_name,
                                           form.instance.offer.auction.contact_email)]
            )

            email.send(fail_silently=True)

        except IndexError:
            pass

        if self.request.is_ajax():
            return JsonResponse({'success': True})

        return response

    def get_initial(self):
        initial = super().get_initial()
        initial.update({'amount': self.get_object().must_bid})
        return initial

    def get_success_url(self):
        return reverse('offer:view', kwargs={'user': self.request.auction.user.slug,
                                             'auction': self.request.auction.slug,
                                             'auctionee': self.get_object().user.slug,
                                             'pk': self.get_object().pk})


class OfferListView(AuctionMixin, TemplateView):
    template_name = 'offer/list.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.auction.complete:
            if request.user.is_anonymous or request.user.slug != self.kwargs.get('auctionee'):
                return redirect(reverse('auction:auction', kwargs={
                    'user': request.auction.user.slug, 'auction': request.auction.slug
                }))

        elif not self.request.auction.categories.exists():
            return redirect(reverse('auction:auction', kwargs={
                'user': request.auction.user.slug, 'auction': request.auction.slug
            }))

        else:
            user_check = any([True for offer in request.auction.offers.all()
                              if offer.user.slug == self.kwargs.get('auctionee')])

            if not user_check and (request.user.is_anonymous or request.user.slug != self.kwargs.get('auctionee')):
                messages.info(request, "{} isn't in the auction".format(self.kwargs.get('auctionee')))
                return redirect(reverse('auction:auction', kwargs={
                    'user': request.auction.user.slug, 'auction': request.auction.slug
                }))

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        if self.request.auction.complete:
            offer_list = []
            for bid in self.request.auction.top_bids:
                if bid.offer.user_id == self.request.user.id:
                    offer_list.append(bid)

            data.update({'offer_list': offer_list})

        else:
            data.update({'offer_list': [offer for offer in self.request.auction.offers.all()
                                        if offer.user.slug == self.kwargs.get('auctionee')]})
        return data

    def get_template_names(self):
        if self.request.user.is_authenticated and self.request.user.slug == self.kwargs.get('auctionee'):
            return ['offer/winners.html' if self.request.auction.complete else 'offer/list.html']
        else:
            return ['offer/browse.html']


class OfferSearchView(AuctionView, FormView):
    form_class = SearchForm

    def get_context_data(self, **kwargs):
        data = super().get_context_data()

        category = self.kwargs.get('category')
        tag = self.kwargs.get('tag')

        offers = data.get('offer_list', [])

        kw = {'user': self.request.auction.user.slug, 'auction': self.request.auction.slug}

        if category:
            kw.update({'category': category})

        if tag:
            kw.update({'tag': tag})

        if category and tag:
            form_action = reverse('offer:search_category_tag', kwargs=kw)
        elif category:
            form_action = reverse('offer:search_category', kwargs=kw)
        elif tag:
            form_action = reverse('offer:search_tag', kwargs=kw)
        else:
            form_action = data.get('form_action')

        if category and not any([True for c in self.request.auction.categories.all() if c.slug == category]):
            category = None

        if category:
            offers = [o for o in offers if o.category.slug == category]

        if tag:
            offers = [o for o in offers if any([True for t in o.tags.all() if t.slug == tag])]

        data.update({
            'offer_list': offers,
            'form_action': form_action
        })

        return data


class OfferCreateView(AuctionMixin, OfferMixin, CreateView):
    model = Offer
    template_name = 'offer/create.html'
    form_class = OfferForm

    def dispatch(self, request, *args, **kwargs):
        if not self.request.auction.categories.exists():
            return redirect(reverse('auction:auction', kwargs={
                'user': request.auction.user.slug, 'auction': request.auction.slug
            }))

        user_offers = [offer for offer in self.request.auction.offers.all() if offer.user.id == self.request.user.id]

        if request.auction.max_offers_per_user and len(user_offers) >= request.auction.max_offers_per_user:
            messages.warning(request, "You have the maximum number of offers in this auction")
            return redirect(self.get_success_url())

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('offer:list', kwargs={
            'user': self.auction.user.slug,
            'auction': self.auction.slug,
            'auctionee': self.request.user.slug
        })

    def form_valid(self, form):
        form.instance.auction = self.request.auction
        form.instance.user = self.request.user

        response = super().form_valid(form)

        message = mark_safe("<p>{} has added an offer to the auction. Go to {}{} to view.</p>".format(
            self.request.user,
            settings.BASE_URL,
            reverse("offer:update_administration", kwargs={
                'auction': self.request.auction.slug,
                'user': self.request.auction.user.slug,
                'pk': form.instance.id
            })
        ))

        self.request.auction.user.email_user("[{}] Offer added".format(self.request.auction.title), message)

        return response


@method_decorator(offer_owner_required, name='dispatch')
class OfferUpdateView(AuctionMixin, OfferMixin, UpdateView):
    model = Offer
    template_name = 'offer/update.html'
    form_class = OfferForm

    def form_valid(self, form):
        messages.success(self.request, 'The offer settings have been updated')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('offer:list', kwargs={
            'user': self.auction.user.slug,
            'auction': self.auction.slug,
            'auctionee': self.object.user.slug
        })


@method_decorator(offer_owner_required, name='dispatch')
class OfferDeleteView(AuctionMixin, DeleteView):
    model = Offer
    template_name = 'offer/delete.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.auction.signups_open:
            messages.info(request, 'Sorry, offers cannot be deleted for this auction')
            return redirect(reverse('offer:list', kwargs={
                'user': request.auction.user.slug,
                'auction': request.auction.slug,
                'auctionee': self.object.user.slug
            }))

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('offer:list', kwargs={
            'user': self.auction.user.slug,
            'auction': self.auction.slug,
            'auctionee': self.object.user.slug
        })


@method_decorator(auction_owner_required, name='dispatch')
class OfferListAdministrationView(AuctionMixin, ListView):
    model = Offer
    template_name = 'offer/administration/list.html'

    def get_queryset(self):
        if self.kwargs.get('category'):
            return self.auction.offers.filter(category__slug=self.kwargs.get('category')).all()

        return self.auction.offers.all()


@method_decorator(auction_owner_required, name='dispatch')
class OfferUpdateAdministrationView(AuctionMixin, OfferMixin, UpdateView):
    model = Offer
    template_name = 'offer/administration/update.html'
    form_class = OfferForm

    def form_valid(self, form):
        messages.success(self.request, 'The offer settings have been updated')
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('offer:list_administration', kwargs={
            'user': self.auction.user.slug,
            'auction': self.auction.slug
        })


@method_decorator(auction_owner_required, name='dispatch')
class OfferDeleteAdministrationView(AuctionMixin, DeleteView):
    model = Offer
    template_name = 'offer/administration/delete.html'

    def get_success_url(self):
        return reverse('offer:list_administration', kwargs={
            'user': self.auction.user.slug,
            'auction': self.auction.slug,
        })
