from django.urls import path

from channels.auth import AuthMiddlewareStack
from channels.routing import (ProtocolTypeRouter, URLRouter)

from adhoc.consumers import ItemConsumer
from offer.consumers import OfferConsumer


application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        URLRouter([
            path('offer/<int:offer_id>/', OfferConsumer),
            path('item/<int:item_id>/', ItemConsumer)
        ])
    ),
})
