from decimal import Decimal

from django.db import models
from django.db.models.functions import Lower
from django.utils.functional import cached_property

from taggit.managers import TaggableManager

from bid.models import Bid
from core.helpers import (file_name, teaser)
from tags.models import FanticsTaggedItem


class OfferManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()\
            .select_related('user') \
            .select_related('category') \
            .prefetch_related('tags') \
            .order_by(Lower('user__display_name')) \
            .annotate(
                count_bids=models.Count('bids', distinct=True)
            )


def offer_image_name(instance, filename):
    return '/'.join(['offers', file_name(filename)])


class Offer(models.Model):
    user = models.ForeignKey('account.User', related_name='offers', on_delete=models.CASCADE)
    auction = models.ForeignKey('auction.Auction', related_name='offers', on_delete=models.CASCADE)
    category = models.ForeignKey('category.Category', related_name='offers', on_delete=models.CASCADE)
    notes = models.TextField(blank=False)
    tags = TaggableManager(through=FanticsTaggedItem, blank=True)
    deadline = models.DateField(null=True, blank=True, help_text='The date by which you will have delivered the offer '
                                                                 'to the winning bidder(s)')
    minimum_bid = models.DecimalField(blank=True, max_digits=8, decimal_places=2, default=Decimal('1.00'))
    quantity = models.IntegerField(default=1, help_text="How many of the item you're offering can be individually bid "
                                                        "on")
    image = models.ImageField(null=True, blank=True, upload_to=offer_image_name)
    information_website = models.URLField(blank=True, max_length=255)

    objects = OfferManager()

    @property
    def teaser(self):
        return teaser(self.notes, 200)

    @cached_property
    def _bids(self):
        return self.bids.all()

    @property
    def top_bids(self):
        return self._bids[:self.quantity] if self.count_bids else []

    @property
    def must_bid(self):
        try:
            return Decimal(self.top_bids[self.quantity-1].amount + 1)
        except IndexError:
            return self.minimum_bid

    @property
    def total(self):
        total = 0

        for bid in self.top_bids:
            if bid.has_donated:
                total += bid.amount

        return total

    def __str__(self):
        return "{}: {} (x{})".format(self.user.display_name, self.category.name, self.quantity)
