from django.conf import settings
from django.contrib.auth.models import (AbstractUser, UserManager as AbstractUserManager)
from django.db import models
from django.db.models.functions import Lower
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from autoslugged import AutoSlugField

from core.helpers import file_name
from message.models import Recipient

from .helpers import ValidatingTokenGenerator


class UserManager(AbstractUserManager):
    def get_queryset(self):
        unread_messages = Recipient.objects\
            .filter(read__isnull=True, user_id=models.OuterRef('pk'))\
            .order_by()\
            .values('user_id')

        return super().get_queryset().annotate(
            count_messages=models.Count('inbox', distinct=True),
            count_unread_messages=models.Subquery(unread_messages.annotate(c=models.Count('*')).values('c'),
                                                  output_field=models.IntegerField())
        ).order_by(Lower('username'))

    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})


def user_file_name(instance, filename):
    return '/'.join(['user', 'icons', file_name(filename)])


class User(AbstractUser):
    display_name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='display_name', unique=True, always_update=True)
    is_validating = models.BooleanField(default=False, help_text='E-mail not confirmed')
    opt_in_email = models.BooleanField(default=False)

    website = models.URLField(blank=True, max_length=255)

    tumblr = models.CharField(blank=True, max_length=255)
    twitter = models.CharField(blank=True, max_length=255)
    livejournal = models.CharField(blank=True, max_length=255)
    dreamwidth = models.CharField(blank=True, max_length=255)
    pillowfort = models.CharField(blank=True, max_length=255)
    ao3 = models.CharField(blank=True, max_length=255)
    instagram = models.CharField(blank=True, max_length=255)

    icon = models.ImageField(null=True, blank=True, upload_to=user_file_name)

    _orphan = None

    objects = UserManager()

    def __str__(self):
        return self.display_name

    def save(self, *args, **kwargs):
        self.email = self.email.lower()

        return super().save(*args, **kwargs)

    @property
    def orphan(self):
        if not self._orphan:
            hash = settings.FANTICS_HASH.encode(self.id)
            self._orphan = User.objects.create_user("orphan-{}".format(hash), "orphan-{}@fantics.me".format(hash))

        return self._orphan

    def delete(self, using=None, keep_parents=False):
        # if they have done absolutely nothing on the site, they can get deleted
        # otherwise we have to create an orphan to inherit
        if not self.auctions.exists() \
                and not self.auctions.exists() \
                and not self.offers.exists() \
                and not self.bids.exists() \
                and not self.outbox.exists() \
                and not self.pinch_hits.exists():

            return super().delete(using=using, keep_parents=keep_parents)

        # if they've sent messages
        if self.outbox.exists():
            outbox = self.outbox.all()
            outbox.update(user=self.orphan)

        # if they've created auction events
        if self.auctions.exists():
            for auction in self.auctions.all():
                if auction.donated:
                    auction.user = self.orphan
                    auction.save()

                else:
                    auction.delete()

        # if they've bid in auction events
        if self.bids.filter(has_donated=True).count():
            bids = self.bids.filter(has_donated=True)
            bids.update(user=self.orphan)

            self.bids.filter(has_donated=False).delete()

        # if they've offered items in auction events
        if self.offers.filter(bids__has_donated=True).count():
            offers = self.offers.filter(bids__has_donated=True)
            offers.update(user=self.orphan)

            self.offers.filter(bids__has_donated=False).delete()

        # if they've pinch-hit
        if self.pinch_hits.exists():
            pinch_hits = self.pinch_hits.all()
            pinch_hits.update(pinch_hitter=self.orphan)

        return super().delete(using=using, keep_parents=keep_parents)

    def send_email_verification(self, request):
        if not self.is_validating:
            return False

        account_activation_token = ValidatingTokenGenerator()

        message = render_to_string('account/emails/verification.html', {
            'user': self,
            'domain': settings.BASE_URL,
            'uid': urlsafe_base64_encode(force_bytes(self.pk)).decode(),
            'token': account_activation_token.make_token(self),
        })

        self.email_user('Activate Your Account', message)

        return message
