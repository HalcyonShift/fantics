from django.contrib import messages
from django.shortcuts import (reverse, redirect)
from django.utils import timezone
from django.views.generic import (ListView, TemplateView)

from auction.models import Auction


class HomeView(ListView):
    template_name = 'main/brochure/home.html'
    model = Auction

    def get_queryset(self):
        return super().get_queryset().filter(bidding_ends__gte=timezone.now(), is_listed=True)


class AuctionModeView(TemplateView):
    template_name = 'main/brochure/auction.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            messages.error(request, "You must be logged in to create an auction")
            return redirect(reverse('account:login'))

        return super().dispatch(request, *args, **kwargs)
