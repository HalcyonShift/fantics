from django import template
from django.contrib.humanize.templatetags.humanize import intcomma
from django.utils import dateformat
from django.utils import timezone
from django.utils.html import mark_safe

from ..settings import CURRENCY_SYMBOLS

register = template.Library()


@register.inclusion_tag('core/forms/_password.html')
def password_show_hide(element):
    return {'element': element}


@register.inclusion_tag('core/forms/_datetimepicker.html')
def datetimepicker(field_name, label=None, value=None, mode="datetime", placeholder=None, required=False):
    if value:
        if mode == "datetime":
            value = dateformat.format(timezone.localtime(value), "m/d/Y H:i")
        elif mode == "date":
            value = dateformat.format(value, "m/d/Y")
        else:
            value = dateformat.format(timezone.localtime(value), "H:i")

    return {'field_name': field_name, 'label': label, 'value': value, 'mode': mode, 'placeholder': placeholder,
            'required': required}


def format_currency(amount, to_code, symbol=True):
    if '.00' in str(amount):
        amount = str(amount).replace('.00', '')
    else:
        amount = round(amount, 2)

    if not symbol:
        return intcomma(amount)

    else:
        return mark_safe('{0}{1}'.format(
            CURRENCY_SYMBOLS.get(to_code, to_code),
            intcomma(amount)
        ))


@register.simple_tag(takes_context=True)
def currency(context, amount, to_code=None, from_code=None, symbol=True):
    if to_code and from_code and to_code != from_code:
        currency = None

        for rate in context['request'].currency:
            if rate.code == from_code:
                currency = rate
                break

        amount = currency.convert(amount, to_code)

    return format_currency(amount, to_code, symbol=symbol)


@register.filter
def pretty_url(value):
    return value.replace("http://", "").replace("https://", "").rstrip("/")


@register.inclusion_tag('core/_list_filter.html', takes_context=True)
def list_filter(context, page_obj, paginator, show_search=False):
    return {'request': context['request'], 'page_obj': page_obj, 'paginator': paginator, 'show_search': show_search}


class EncryptEmail(template.Node):
    def __init__(self, context_var):
        self.context_var = template.Variable(context_var)  # context_var

    def render(self, context):
        import random
        email_address = self.context_var.resolve(context)
        character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz'
        char_list = list(character_set)
        random.shuffle(char_list)

        key = ''.join(char_list)

        cipher_text = ''
        id = 'e' + str(random.randrange(1, 999999999))

        for a in email_address:
            cipher_text += key[character_set.find(a)]

        script = 'var a="' + key + '";var b=a.split("").sort().join("");var c="' + cipher_text + '";var d="";'
        script += 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));'
        script += 'document.getElementById("' + id + '").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"'

        script = "eval(\"" + script.replace("\\", "\\\\").replace('"', '\\"') + "\")"
        script = '<script type="text/javascript">/*<![CDATA[*/' + script + '/*]]>*/</script>'

        return '<span id="' + id + '">[javascript protected email address]</span>' + script


def encrypt_email(parser, token):
    """
        {% encrypt_email user.email %}
    """

    tokens = token.contents.split()
    if len(tokens) != 2:
        raise template.TemplateSyntaxError("%r tag accept two argument" % tokens[0])
    return EncryptEmail(tokens[1])


register.tag('encrypt_email', encrypt_email)
