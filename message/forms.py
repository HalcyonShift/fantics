from django import forms

from account.models import User

from .models import Message


class MessageForm(forms.ModelForm):
    to = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Comma-separated list of names'}))

    class Meta:
        model = Message
        fields = ['subject', 'text', ]

    def clean_to(self):
        users = [user.strip() for user in self.cleaned_data.get('to').split(',')]
        users = User.objects.filter(display_name__iregex=r'(' + '|'.join(users) + ')')

        if not users:
            raise forms.ValidationError('No valid recipients have been found', 'invalid', params=['to'])

        return users
