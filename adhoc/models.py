import pytz

from decimal import Decimal

from django.conf import settings
from django.db import models
from django.utils import (timezone, dateformat)
from django.utils.functional import cached_property

from core.models import Currency


class ItemManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()\
            .filter(is_active=True)\
            .order_by('-bidding_ends')\
            .select_related('user')\
            .annotate(
                count_bids=models.Count('bids', distinct=True),
                currency_rate=models.Subquery(Currency.objects.filter(code=models.OuterRef('currency')).values('rate')),
            ).distinct()


class Item(models.Model):
    title = models.CharField(max_length=254)
    user = models.ForeignKey('account.User', related_name='adhoc_auctions', on_delete=models.CASCADE)
    is_listed = models.BooleanField(default=True, db_index=True,
                                    help_text='Show on your profile and other public areas')
    is_active = models.BooleanField(default=True, db_index=True)
    bidding_starts = models.DateTimeField(null=True, blank=True)
    bidding_ends = models.DateTimeField(null=True, blank=True)

    currency = models.CharField(max_length=3, default="USD")
    timezone = models.CharField(choices=[(tz, tz) for tz in pytz.common_timezones], max_length=255, default="UTC")

    beneficiary_name = models.CharField(max_length=255)
    beneficiary_website = models.URLField(max_length=255, blank=True)

    category = models.CharField(max_length=10, choices=[
        ('art', 'Art'),
        ('crafts', 'Crafts'),
        ('fiction', 'Fiction'),
        ('video', 'Video'),
        ('other', 'Other')
    ], default='art', db_index=True)
    notes = models.TextField(blank=False)
    minimum_bid = models.DecimalField(blank=True, max_digits=8, decimal_places=2, default=Decimal('1.00'))
    quantity = models.IntegerField(default=1, help_text="How many of the item you're offering can be individually bid "
                                                        "on")
    information_link = models.URLField(blank=True, max_length=254)

    created = models.DateTimeField(auto_now_add=True)

    objects = ItemManager()

    def __str__(self):
        return self.title

    @property
    def exchange(self):
        return Currency(code=self.currency, rate=self.currency_rate)

    @property
    def hash(self):
        return settings.FANTICS_HASH.encode(self.id)

    @cached_property
    def _bids(self):
        return self.bids.all()

    @staticmethod
    def _period(starts, ends):
        if not starts or not ends:
            return None

        if starts.month == ends.month and starts.year == ends.year:
            return "{} to {}".format(
                dateformat.format(starts, "jS"),
                dateformat.format(ends, "jS M Y"),
            )

        elif starts.year == ends.year:
            return "{} to {}".format(
                dateformat.format(starts, "jS M"),
                dateformat.format(ends, "jS M Y")
            )

        else:
            return "{} - {}".format(
                dateformat.format(starts, "jS M Y"),
                dateformat.format(ends, "jS M Y")
            )

    @property
    def bidding_period(self):
        return self._period(self.bidding_starts, self.bidding_ends)

    @property
    def bidding_open(self):
        if not self.bidding_starts \
                or not self.bidding_ends \
                or not (self.bidding_starts <= timezone.now() <= self.bidding_ends):
            return False

        return True

    @property
    def pending(self):
        if not self.bidding_open:
            return True

        return False

    @property
    def complete(self):
        return bool(self.bidding_ends and timezone.now() > self.bidding_ends)

    @cached_property
    def top_bids(self):
        return self._bids[:self.quantity] if self.count_bids else []

    @property
    def must_bid(self):
        try:
            return Decimal(self.top_bids[self.quantity-1].amount + 1)
        except IndexError:
            return self.minimum_bid


class BidManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related('user').order_by('-amount')


class Bid(models.Model):
    user = models.ForeignKey('account.User', related_name='adhoc_bids', on_delete=models.CASCADE)
    anonymous = models.BooleanField(default=False)
    item = models.ForeignKey('adhoc.Item', related_name='bids', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(blank=True, max_digits=6, decimal_places=2, default=Decimal('0.00'))
    has_donated = models.BooleanField(default=False, db_index=True)
    receipt = models.TextField(blank=True)

    objects = BidManager()

    def __str__(self):
        return str(self.amount)

    @property
    def hash(self):
        return settings.FANTICS_HASH.encode(self.id)
