# README #

## Site ##
https://fantics.herokuapp.com/

## Installation ##

### Virtualenv ###

``` sh
$ cd /your/venv/directory/
$ virtualenv -p python3.6.7 fantics
$ source fantics/bin/activate
$ cd /your/code/directory/
$ git clone https://bitbucket.org/HalcyonShift/fantics.git
$ cd fantics
$ pip install -r requirements.txt
```

### Settings ###
Create fantics/.env, amending as appropriate:

```
SECRET_KEY=h84jd9y3ft1+*o#z(q%tomc4p1zuq_@m1(&3sd(8emf)kq9=g(
DEBUG=1
DATABASE_ENGINE=django.db.backends.postgresql
DATABASE_HOST=localhost
DATABASE_NAME=fantics
DATABASE_USER=fantics
DATABASE_PASSWORD=fantics
DATABASE_PORT=5432
EMAIL_BACKEND=django.core.mail.backends.console.EmailBackend
ENVIRONMENT=local
ALLOWED_HOSTS=127.0.0.1
BASE_URL=http://127.0.0.1:8003
```

## Testing + Quality Control ##

### Unit Tests ###
```sh
$ python manage.py jenkins account adhoc auction bid category core main message offer tags --enable-coverage --coverage-format=html
```
```sh
$ python manage.py jenkins account adhoc auction bid category core main message offer tags --enable-coverage
```

### SonarQube Server ###
* Follow the installation instructions at https://docs.sonarqube.org/7.4/setup/install-server/
* Create fantics project at http://localhost:9000

```sh
$ /path/to/sonarqube/bin/macosx-universal-64/sonar.sh console
$ cd /your/code/directory/fantics
$ sonar-scanner \
-Dsonar.projectKey=fantics \
-Dsonar.sources=. \
-Dsonar.host.url=http://localhost:9000 \
-Dsonar.login=[login_key] \
-Dsonar.python.coverage.reportPath=reports/coverage.xml \
-Dsonar.python.xunit.reportPath=reports/junit.xml \
-Dsonar.coverage.exclusions=venv/**,fantics/**,runtime.txt,Procfile,**/migrations/**,reports/** \
-Dsonar.exclusions=.scannerwork/**,venv/**,fantics/**,runtime.txt,Procfile,**/migrations/**,reports/*
```

## CI/CD ##

### Jenkins ###
Follow the installation instructions at https://jenkins.io/doc/book/installing/

Go to Manage Jenkins > Manage Plugins > Available
- Bitbucket Branch Source Plugin
- Bitbucket Plugin
- Build Authorization Token Root Plugin
- Build Timeout
- Cobertura Plugin
- Hudson Post build task
- Maven Integration plugin
- Pipeline
- Pipeline: GitHub Groovy Libraries
- pyenv plugin
- Robot Framework plugin
- SonarQube Scanner for Jenkins

Go to Manage Jenkins > Configure System > SonarQube Servers
- Enable injection of SonarQube server configuration as build environment variables
- Name: SonarQube
- Server URL: http://localhost:9000
- Authentication token: [login_key]

Go to Manage Jenkins > Configure System > SonarQube Servers
- Enable injection of SonarQube server configuration as build environment variables
- Name: SonarQube
- Server URL: http://localhost:9000
- Authentication token: [login_key]

Go to Manage Jenkins > Configure Global Security
- Authorization: Anyone can do anything (if you're in a secure environment!!)

Go to Manage Jenkins > Global Tool Configuation > SonarQube Scanner
- Add SonarQube Scanner
- Name: SonarQube
- Install automatically
- Install from Maven Central

Go to New Item
- Create Multibranch pipeline
- Branch sources: Bitbucket
- Remove fork behaviour
- Filter by name (with wildcards): dev_*

### SonarQube ###
Go to Administration > Configuration > Webhooks
- URL: http://jenkins.url/sonarqube-webhook/

### ngrok ###
Install ngrok following instructions at https://ngrok.com/
```sh
$ ./ngrok http 80
```

### BitBucket ###
Go to Settings > Webhooks
- URL: https://[ngrok.url]/bitbucket-scmsource-hook/notify
