from decimal import Decimal

from django.conf import settings
from django.core.mail import EmailMessage
from django.db import models
from django.shortcuts import reverse
from django.utils import (timezone, dateformat)

from django_q.tasks import async_task

from core.templatetags.core_tags import format_currency


class BidManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()\
            .select_related('user') \
            .select_related('pinch_hitter') \
            .select_related('offer') \
            .select_related('offer__category') \
            .select_related('offer__user') \
            .order_by('-amount')


class Bid(models.Model):
    MODES = [
        'donation_complete',
        'donation_unverified',
        'pending_alert',
        'pending_donation',
        'donation_late'
    ]

    user = models.ForeignKey('account.User', related_name='bids', on_delete=models.CASCADE)
    pinch_hitter = models.ForeignKey('account.User', related_name='pinch_hits', on_delete=models.CASCADE, null=True,
                                     blank=True)
    anonymous = models.BooleanField(default=False)
    offer = models.ForeignKey('offer.Offer', related_name='bids', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    amount = models.DecimalField(blank=True, max_digits=6, decimal_places=2, default=Decimal('0.00'))
    donation_deadline = models.DateTimeField(blank=True, null=True)
    winner_alert_email = models.TextField(blank=True)
    winner_alert_email_date = models.DateTimeField(blank=True, null=True)
    auctionee_sent_winner_details_email = models.TextField(blank=True)
    auctionee_sent_winner_details_email_date = models.DateTimeField(blank=True, null=True)
    winner_sent_auctionee_details_email = models.TextField(blank=True)
    winner_sent_auctionee_details_email_date = models.DateTimeField(blank=True, null=True)
    pinch_hitter_sent_winner_details_email = models.TextField(blank=True)
    pinch_hitter_sent_winner_details_email_date = models.DateTimeField(blank=True, null=True)
    winner_sent_pinch_hitter_details_email = models.TextField(blank=True)
    winner_sent_pinch_hitter_details_email_date = models.DateTimeField(blank=True, null=True)

    donated = models.DateTimeField(blank=True, null=True)
    receipt = models.TextField(blank=True)
    has_donated = models.BooleanField(default=False, db_index=True)

    objects = BidManager()

    def __str__(self):
        return str(self.amount)

    @property
    def donate_by(self):
        return self.donation_deadline or self.offer.auction.donation_deadline

    def can_donate(self, top_bids):
        check_valid = any([True for b in top_bids if b.id == self.id])
        return True if check_valid and self.donate_by > timezone.now() else False

    @property
    def hash(self):
        return settings.FANTICS_HASH.encode(self.id)

    @property
    def status(self):
        for state in ['donation_complete', 'donation_unverified', 'pending_alert', 'pending_donation', 'donation_late']:
            if getattr(self, state):
                return state

    @property
    def donation_complete(self):
        # donation confirmed
        return self.has_donated

    @property
    def donation_unverified(self):
        # no confirmation, but there is a receipt to confirm, so even if it's post deadline we're good
        return True if not self.donation_complete and self.receipt else False

    @property
    def pending_alert(self):
        # no confirmation, no receipt, but also no e-mail went out so deadline doesn't apply
        return True if not self.donation_complete \
                       and not self.donation_unverified \
                       and not self.winner_alert_email_date else False

    @property
    def pending_donation(self):
        # no confirmation, no receipt, email went out and we're inside the deadline, so we're waiting
        return True if not self.donation_complete \
                       and not self.donation_unverified \
                       and not self.pending_alert \
                       and timezone.now() < self.donate_by else False

    @property
    def donation_late(self):
        # no confirmation, no receipt, email went out and we're outside the deadline, so it's late
        return True if not self.donation_complete \
                       and not self.donation_unverified \
                       and not self.pending_alert \
                       and timezone.now() > self.donate_by else False

    def generate_winner_donation_alert(self):
        email = self.offer.auction.winner_alert_email_body.format(**{
            'bidder': self.user.display_name,
            'auctionee': self.offer.user.display_name,
            'category': self.offer.category.name,
            'amount': "{} ({})".format(
                format_currency(self.amount, self.offer.auction.currency),
                self.offer.auction.currency
            ),
            'deadline': "{} ({})".format(
                dateformat.format(timezone.localtime(self.donate_by), "m/d/Y H:i"),
                self.offer.auction.timezone
            ),
            'donation_instructions': self.offer.auction.beneficiary_donation_information,
            'donation_website': self.offer.auction.beneficiary_donation_website,
            'link': "{}{}".format(settings.BASE_URL, reverse('bid:donate', kwargs={
                'user': self.offer.auction.user.slug,
                'auction': self.offer.auction.slug,
                'bidder': self.offer.user.slug,
                'hash': self.hash
            }))
        })

        return str(email)

    def _auctionee_details_email(self, email):
        return str(email.format(**{
            'bidder': self.user.display_name,
            'auctionee': self.offer.user.display_name,
            'email': self.offer.user.email,
            'category': self.offer.category.name,
            'amount': "{} ({})".format(
                format_currency(self.amount, self.offer.auction.currency),
                self.offer.auction.currency
            )
        }))

    def _pinch_hitter_details_email(self, email):
        return str(email.format(**{
            'bidder': self.user.display_name,
            'auctionee': self.offer.user.display_name,
            'pinch_hitter': self.pinch_hitter.display_name,
            'email': self.offer.user.email,
            'category': self.offer.category.name,
            'amount': "{} ({})".format(
                format_currency(self.amount, self.offer.auction.currency),
                self.offer.auction.currency
            )
        }))

    # the auctionee's details to send to the winning bidder
    def generate_winner_sent_auctionee_details_email(self):
        return self._auctionee_details_email(self.offer.auction.auctionee_details_to_winner_email_body)

    # the winning bidder's details to send to the auctionee
    def generate_auctionee_sent_winner_details_email(self):
        return self._auctionee_details_email(self.offer.auction.winner_details_to_auctionee_email_body)

    # the pinch-hitter's details to send to the winning bidder
    def generate_winner_sent_pinch_hitter_details_email(self):
        return self._pinch_hitter_details_email(self.offer.auction.pinch_hitter_details_to_winner_email_body)

    # the winning bidder's details to send to the pinch-hitter
    def generate_pinch_hitter_sent_winner_details_email(self):
        return self._pinch_hitter_details_email(self.offer.auction.winner_details_to_pinch_hitter_email_body)

    # send e-mail to winning bidder with donation information
    def email_winner_donation_alert(self, async=False):
        subject = "[{}] {}".format(self.offer.auction.title, self.offer.auction.winner_alert_email_subject)
        from_email = "<Fantics> {}".format(settings.DEFAULT_FROM_EMAIL)
        to_email = "<{}> {}".format(self.user.display_name, self.user.email)
        reply_to = "<{}> {}".format(self.offer.auction.contact_name, self.offer.auction.contact_email)

        if not async:
            email = EmailMessage(subject, self.winner_alert_email, from_email, to=[to_email, ], reply_to=[reply_to, ])

            return email.send(fail_silently=True)

        else:
            async_task('core.tasks.send_email', subject, self.winner_alert_email, from_email, to_email, reply_to)

    # send e-mail to auctionee with winning bidder details
    def email_winner_details_to_auctionee(self):
        email = EmailMessage(
            "[{}] {}".format(self.offer.auction.title, self.offer.auction.winner_details_to_auctionee_email_subject),
            self.auctionee_sent_winner_details_email,
            "<Fantics> {}".format(settings.DEFAULT_FROM_EMAIL),
            to=[self.user.email],
            reply_to=["<{}> {}".format(self.offer.auction.contact_name, self.offer.auction.contact_email)]
        )

        return email.send(fail_silently=True)

    # send e-mail to winning bidder with auctionee details
    def email_auctionee_details_to_winner(self):
        email = EmailMessage(
            "[{}] {}".format(self.offer.auction.title, self.offer.auction.auctionee_details_to_winner_email_subject),
            self.winner_sent_auctionee_details_email,
            "<Fantics> {}".format(settings.DEFAULT_FROM_EMAIL),
            to=[self.user.email],
            reply_to=["<{}> {}".format(self.offer.auction.contact_name, self.offer.auction.contact_email)]
        )

        return email.send(fail_silently=True)

    # send e-mail to pinch hitter with winning bidder details
    def email_winner_details_to_pinch_hitter(self):
        email = EmailMessage(
            "[{}] {}".format(self.offer.auction.title, self.offer.auction.winner_details_to_pinch_hitter_email_subject),
            self.pinch_hitter_sent_winner_details_email,
            "<Fantics> {}".format(settings.DEFAULT_FROM_EMAIL),
            to=[self.user.email],
            reply_to=["<{}> {}".format(self.offer.auction.contact_name, self.offer.auction.contact_email)]
        )

        return email.send(fail_silently=True)

    # send e-mail to winning bidder with pinch hitter details
    def email_pinch_hitter_details_to_winner(self):
        email = EmailMessage(
            "[{}] {}".format(self.offer.auction.title, self.offer.auction.pinch_hitter_details_to_winner_email_subject),
            self.winner_sent_pinch_hitter_details_email,
            "<Fantics> {}".format(settings.DEFAULT_FROM_EMAIL),
            to=[self.user.email],
            reply_to=["<{}> {}".format(self.offer.auction.contact_name, self.offer.auction.contact_email)]
        )

        return email.send(fail_silently=True)
