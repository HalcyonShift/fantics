from django.conf import settings


def environment_settings(request):
    return {
        'BASE_URL':  settings.BASE_URL,
        'ENVIRONMENT': settings.ENVIRONMENT
    }
