import pytz

from decimal import Decimal

from django.db import models
from django.utils import (timezone, dateformat)
from django.utils.functional import cached_property

from autoslugged import AutoSlugField

from category.models import Category
from core.models import Currency
from core.helpers import file_name
from offer.models import Offer


class AuctionQuerySet(models.QuerySet):
    def with_related_offers(self):
        return self \
            .prefetch_related('categories')\
            .prefetch_related('offers') \
            .prefetch_related('offers__category') \
            .prefetch_related('offers__user') \
            .prefetch_related('offers__tags') \
            .prefetch_related('offers__bids')


class AuctionManager(models.Manager):
    def get_queryset(self):
        return AuctionQuerySet(self.model, using=self._db)\
            .filter(is_active=True)\
            .order_by('-bidding_ends')\
            .select_related('user')\
            .annotate(
                currency_rate=models.Subquery(Currency.objects.filter(code=models.OuterRef('currency')).values('rate')),
                count_categories=models.Count('categories', distinct=True),
                count_offers=models.Count('offers', distinct=True),
            ).distinct()

    def with_related_offers(self):
        return self.get_queryset().with_related_offers()


def auction_icon_name(instance, filename):
    return '/'.join(['auction', 'icons', file_name(filename)])


def auction_logo_name(instance, filename):
    return '/'.join(['auction', 'logo', file_name(filename)])


def auction_banner_name(instance, filename):
    return '/'.join(['auction', 'banners', file_name(filename)])


def auction_beneficiary_name(instance, filename):
    return '/'.join(['auction', 'beneficiaries', file_name(filename)])


class Auction(models.Model):
    user = models.ForeignKey('account.User', related_name='auctions', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from='title', unique_with='user', always_update=True)

    contact_name = models.CharField(max_length=255)
    contact_email = models.EmailField(max_length=255)

    css = models.TextField(blank=True)
    header = models.TextField(blank=True)
    footer = models.TextField(blank=True)

    icon = models.ImageField(null=True, blank=True, upload_to=auction_icon_name, help_text='max. 100x100')
    logo = models.ImageField(null=True, blank=True, upload_to=auction_logo_name, help_text='max. 300x100')
    banner = models.ImageField(null=True, blank=True, upload_to=auction_banner_name, help_text='max. 1140x200')

    is_listed = models.BooleanField(default=True, db_index=True,
                                    help_text='Show on your profile and other public areas')
    is_active = models.BooleanField(default=True, db_index=True)

    signup_starts = models.DateTimeField(null=True, blank=True)
    signup_ends = models.DateTimeField(null=True, blank=True)
    bidding_starts = models.DateTimeField(null=True, blank=True)
    bidding_ends = models.DateTimeField(null=True, blank=True)
    offer_deadline = models.DateField(null=True, blank=True)
    donation_deadline = models.DateTimeField(null=True, blank=True)

    signup_email_subject = models.CharField(blank=True, max_length=255)
    signup_email_body = models.TextField(blank=True)

    bid_email_subject = models.CharField(blank=True, max_length=255)
    bid_email_body = models.TextField(blank=True)

    overbid_email_subject = models.CharField(blank=True, max_length=255)
    overbid_email_body = models.TextField(blank=True)

    winner_alert_email_subject = models.CharField(blank=True, max_length=255)
    winner_alert_email_body = models.TextField(blank=True)

    winner_details_to_auctionee_email_subject = models.CharField(blank=True, max_length=255)
    winner_details_to_auctionee_email_body = models.TextField(blank=True)

    auctionee_details_to_winner_email_subject = models.CharField(blank=True, max_length=255)
    auctionee_details_to_winner_email_body = models.TextField(blank=True)

    pinch_hitter_details_to_winner_email_subject = models.CharField(blank=True, max_length=255)
    pinch_hitter_details_to_winner_email_body = models.TextField(blank=True)

    winner_details_to_pinch_hitter_email_subject = models.CharField(blank=True, max_length=255)
    winner_details_to_pinch_hitter_email_body = models.CharField(blank=True, max_length=255)

    allow_custom_offer_deadline = models.BooleanField(default=False,
                                                      help_text='The person making the offer sets their own deadline')

    max_offers_per_user = models.IntegerField(null=True, blank=True)
    max_quantity_per_offer = models.IntegerField(null=True, blank=True)

    currency = models.CharField(max_length=3, default="USD")
    timezone = models.CharField(choices=[(tz, tz) for tz in pytz.common_timezones], max_length=255, default="UTC")

    beneficiary_name = models.CharField(max_length=255)
    beneficiary_description = models.TextField(blank=True)
    beneficiary_website = models.URLField(max_length=255, blank=True)
    beneficiary_image = models.ImageField(null=True, blank=True, upload_to=auction_beneficiary_name,
                                          help_text='max. 600x600')
    beneficiary_donation_website = models.URLField(max_length=255, blank=True)
    beneficiary_donation_information = models.TextField(blank=True)

    about = models.TextField(blank=True)

    target = models.DecimalField(default=Decimal("0.00"), decimal_places=2, max_digits=8)

    created = models.DateTimeField(auto_now_add=True)

    objects = AuctionManager()

    def __str__(self):
        return self.title

    @property
    def exchange(self):
        return Currency(code=self.currency, rate=self.currency_rate)

    @staticmethod
    def _period(starts, ends):
        if not starts or not ends:
            return None

        if starts.month == ends.month and starts.year == ends.year:
            return "{} to {}".format(
                dateformat.format(starts, "jS"),
                dateformat.format(ends, "jS M Y"),
            )

        elif starts.year == ends.year:
            return "{} to {}".format(
                dateformat.format(starts, "jS M"),
                dateformat.format(ends, "jS M Y")
            )

        else:
            return "{} - {}".format(
                dateformat.format(starts, "jS M Y"),
                dateformat.format(ends, "jS M Y")
            )

    @property
    def auction_period(self):
        return self._period(self.signup_starts, self.bidding_ends)

    @property
    def signup_period(self):
        return self._period(self.signup_starts, self.signup_ends)

    @property
    def bidding_period(self):
        return self._period(self.bidding_starts, self.bidding_ends)

    @property
    def signups_open(self):
        if not self.signup_starts \
                or not self.signup_ends \
                or not (self.signup_starts <= timezone.now() <= self.signup_ends):
            return False

        return True

    @property
    def bidding_open(self):
        if not self.bidding_starts \
                or not self.bidding_ends \
                or not (self.bidding_starts <= timezone.now() <= self.bidding_ends):
            return False

        return True

    @property
    def pending(self):
        if not self.auction_period \
                or not self.beneficiary_donation_information \
                or not self.beneficiary_donation_website \
                or (self.signup_starts and self.signup_starts > timezone.now()):
            return True

        return False

    @property
    def complete(self):
        return bool(self.bidding_ends and timezone.now() > self.bidding_ends)

    @property
    def show_nav_offers(self):
        return bool(self.signup_starts and timezone.now() > self.signup_starts) \
               and not self.show_nav_winners \
               and self.categories.exists()

    @property
    def show_nav_bids(self):
        return bool(self.bidding_starts and timezone.now() > self.bidding_starts) and not self.show_nav_wins

    @property
    def show_nav_wins(self):
        return bool(self.bidding_ends and timezone.now() > self.bidding_ends) and self.top_bids

    @property
    def show_nav_winners(self):
        return bool(self.bidding_ends and timezone.now() > self.bidding_ends)

    @property
    def can_send_email_winner_alerts(self):
        if not self.complete or not self.top_bids or any([True for b in self.top_bids if b.winner_alert_email_date]):
            return False

        return True

    @cached_property
    def top_bids(self):
        bids = []

        for offer in self.offers.all():
            if offer.top_bids:
                bids = bids + offer.top_bids

        return bids

    @cached_property
    def total(self):
        total = 0

        for offer in self.offers.all():
            for bid in offer.top_bids:
                total += bid.amount

        return total

    @cached_property
    def donated(self):
        total = 0

        for offer in self.offers.all():
            for bid in offer.top_bids:
                if bid.has_donated:
                    total += bid.amount

        return total

    @property
    def is_archived(self):
        return bool(self.donated and self.donated == self.total)
